安裝iris
go get github.com/kataras/iris/v12@master

安裝redis
go get github.com/go-redis/redis/v8

安裝toml
go get github.com/BurntSushi/toml

安裝gorm (windos需安裝gcc 64位元 , 設定系統path路徑)
go get -u gorm.io/gorm
go get -u gorm.io/driver/sqlite

http://mingw-w64.org/doku.php/download/mingw-builds

安裝多語系
go get github.com/iafan/go-l10n/loc

前台 build 
cd ./PortalFront
go build front.go

前台 dev 
cd ./PortalFront
go run front.go

後台 build 
cd ./PortalBackend
go build backend.go

後台 dev 
cd ./PortalBackend
go run backend.go

