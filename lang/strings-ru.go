package lang

func init() {
	Loc.Resources["ru"] = map[string]string{
		// Page title
		"Hello": "Здравствуйте!",

		// {N} is the number of messages
		"YouHaveNMessages": "У вас {N} {N_PLURAL:сообщение|сообщения|сообщений}",
	}
}
