package model

import "time"

type AdminRouters struct {
	ID                int
	RouterType        int
	RouterName        string
	RouterPath        string
	RouterPermissions string
	CreatedAt         time.Time
	UpdatedAt         time.Time
	DeletedAt         *time.Time
}
