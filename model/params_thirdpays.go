package model

type ParamsThirdpays struct {
	ID        int
	Name      string
	Merid     string
	Addcrykey string
	Area      string
	MinAmount int
	MaxAmount int
	// CreatedAt time.Time
	// UpdatedAt time.Time
	// DeletedAt *time.Time
}
