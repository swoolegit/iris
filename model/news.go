package model

import "time"

type News struct {
	ID        int
	Title     string
	Content   string
	Enable    int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
