package model

import "time"

type Members struct {
	ID           int
	ParentID     int
	Parents      string
	Enable       int
	FanDian      float64
	Username     string
	Password     string
	CoinPassword string
	Memo         string
	Phone        string
	Line         string
	Email        string
	LoginIP      string
	RegIP        string
	Coin         float64
	InviteLink   InviteLinks `gorm:"foreignKey:ID"`
	LoginAt      time.Time
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
}
