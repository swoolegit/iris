package model

import "time"

type InviteLinks struct {
	ID        int
	Enable    int
	UID       int `gorm:"primaryKey"`
	Code      string
	Type      int
	Used      int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
