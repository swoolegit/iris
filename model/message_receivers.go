package model

import "time"

type MessageReceivers struct {
	ID            int
	MessageID     int
	UID           int
	Username      string
	IsReaded      int
	MessageSender MessageSenders `gorm:"foreignKey:MessageID"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	//DeletedAt     *time.Time
}
