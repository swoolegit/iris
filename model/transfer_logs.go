package model

import "time"

type TransferLog struct {
	ID        int
	UID       int
	Username  string
	Coin      float64
	UserCoin  float64
	Type      int
	ParentID  int
	ExtID     int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
