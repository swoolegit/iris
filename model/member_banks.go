package model

type MemberBanks struct {
	ID          int
	UID         int
	Enable      int
	BankID      int
	BankName    string
	Username    string
	Account     string
	Accountname string
	// CreatedAt time.Time
	// UpdatedAt time.Time
	// DeletedAt *time.Time
}
