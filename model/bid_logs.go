package model

import "time"

type BidLogs struct {
	ID        int
	OrderNo   string
	UID       int
	Username  string
	ProdID    int
	ProdName  string
	Coin      float64
	UserCoin  float64
	Status    int
	ParentID  int
	Parent2ID int
	Parent3ID int
	Winer     int
	EndTime   time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
