package model

import "time"

type AdminMembers struct {
	ID        int
	GroupID   int
	Enable    int
	Username  string
	Password  string
	Memo      string
	LoginIP   string
	LoginAt   time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
