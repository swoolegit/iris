package model

import "time"

type ReportDays struct {
	ID             int
	RegUser        int
	RechargeUser   int
	LoginUser      int
	BidUser        int
	BidAmount      float64
	BonusAmount    float64
	RebateAmount   float64
	RechargeAmount float64
	CashAmount     float64
	BrokerAmount   float64
	Date           time.Time
}
