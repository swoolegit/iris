package model

type MemberReports struct {
	ID       int
	UID      int
	Recharge float64
	Cash     float64
	Rebate   float64
	Bonus    float64
	Bid      float64
	Croker   float64
	//ActionDate time.Time
	ActionDate string `json:"actionDate" gorm:"column:actionDate"`
}
