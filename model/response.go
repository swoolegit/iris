package model

type Response struct {
	Status int         `json:"status"` //接口状态 1 true , 0 false
	Msg    interface{} `json:"msg"`    // 接口信息
	Data   interface{} `json:"data"`   //接口数据
}
