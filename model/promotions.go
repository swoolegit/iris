package model

import "time"

type Promotions struct {
	ID        int
	Title     string
	Content   string
	Img       string
	Enable    int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
