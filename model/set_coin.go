package model

type SetCoin struct {
	UID   int
	Coin  float64
	Type  int
	ExtID int64
}
