package model

import "time"

type Videos struct {
	ID        int
	Title     string
	Url       string
	Enable    int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
