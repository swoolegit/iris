package model

import "time"

type MemberRecharges struct {
	ID                int
	UID               int
	RechargeID        int64
	Username          string
	Amount            float64
	RechargeModel     int
	RechargeModelName string
	RechargeComID     int
	RechargeComName   string
	ActionIP          string
	Status            int
	Info              string
	Memo              string
	CreatedAt         time.Time
	UpdatedAt         time.Time
	DeletedAt         *time.Time
}
