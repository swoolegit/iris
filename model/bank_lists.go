package model

import "time"

type BankLists struct {
	ID        int
	Name      string
	Ename     string
	Code      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
