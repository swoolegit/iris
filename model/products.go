package model

import "time"

type Products struct {
	ID        int
	Name      string
	Price     float64
	MaxPrice  float64
	BonusDay  int
	Bonus     float64
	Cost      float64
	Enable    int
	Ending    string
	Note      string
	Fee       float64
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
