package model

type ParamsBanks struct {
	ID          int
	BankName    string
	AccountName string
	Account     string
	Remark      string
	Qrcode      string
	MinAmount   int
	MaxAmount   int
	// CreatedAt   time.Time
	// UpdatedAt   time.Time
	// DeletedAt   *time.Time
}
