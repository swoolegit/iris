package model

import "time"

type AdminGroups struct {
	ID               int
	GroupName        string
	GroupPermissions string
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        *time.Time
}
