package model

import "time"

type MessageSenders struct {
	ID        int
	UID       int
	Username  string
	Title     string
	Content   string
	CreatedAt time.Time
	UpdatedAt time.Time
	//DeletedAt *time.Time
}
