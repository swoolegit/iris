package model

type PreBonus struct {
	UID   int
	Coin  float64
	Type  int
	ExtID int
}
