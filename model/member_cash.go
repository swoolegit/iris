package model

import "time"

type MemberCashs struct {
	ID          int
	UID         int
	Username    string
	Amount      int
	BankID      int
	BankName    string
	Account     string
	Accountname string
	Status      int
	Info        string
	OrderNo     string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}
