package model

import (
	"time"
)

type MemberProds struct {
	ID        int
	UID       int
	ProdID    int
	Name      string
	Price     float64
	MaxPrice  float64
	BonusDay  time.Time
	Bonus     float64
	Status    int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
