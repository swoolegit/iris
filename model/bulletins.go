package model

import "time"

type Bulletins struct {
	ID        int
	Enable    int
	Title     string
	Content   string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
