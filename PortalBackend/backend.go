package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/robfig/cron"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	"github.com/kataras/iris/v12/sessions/sessiondb/redis"
	"iris.wa/backend/activity"
	"iris.wa/backend/admin"
	"iris.wa/backend/business"
	"iris.wa/backend/report"
	"iris.wa/backend/system"
	"iris.wa/backend/task"
	"iris.wa/backend/user"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
)

const maxSize = 2 << 20

func main() {
	f := newLogFile()
	defer f.Close()

	app := iris.Default()
	app.Logger().SetOutput(f)

	// 設定conf
	conf.GetInstance().InitConf()

	// DB初始化
	DbStatus := db.GetInstance().InitDataPool(conf.Conf.DB.Source)
	if !DbStatus {
		fmt.Println("failed to connect database:")
		return
	}
	// dbForSess := redis.New(redis.Config{
	// 	Network:   "tcp",
	// 	Addr:      getenv("REDIS_ADDR", "127.0.0.1:6379"),
	// 	Timeout:   time.Duration(30) * time.Second,
	// 	MaxActive: 10,
	// 	Username:  "",
	// 	Password:  "",
	// 	Database:  "",
	// 	Prefix:    "myapp-",
	// 	Driver:    redis.GoRedis(), // defaults.
	// })
	// 開始計畫任務 begin
	// ┌─────────────second 範圍 (0 - 60)
	// │ ┌───────────── min (0 - 59)
	// │ │ ┌────────────── hour (0 - 23)
	// │ │ │ ┌─────────────── day of month (1 - 31)
	// │ │ │ │ ┌──────────────── month (1 - 12)
	// │ │ │ │ │ ┌───────────────── day of week (0 - 6) (0 to 6 are Sunday to
	// │ │ │ │ │ │                  Saturday)
	// │ │ │ │ │ │
	// │ │ │ │ │ │
	// * * * * * *
	c := cron.New()
	//spec := "*/10 * * * * *"
	c.AddFunc("0 0 * * * *", task.Close)    // 結標
	c.AddFunc("0 0 2 * * *", task.Bonus)    // 發利息
	c.AddFunc("0 0 3 * * *", task.Analysis) // 算報表
	c.Start()
	//select {}
	// 開始計畫任務 end

	dbForSess := redis.New(redis.Config{
		Network:   "tcp",
		Addr:      getenv("REDIS_ADDR", conf.Conf.Redis.Source),
		Timeout:   time.Duration(600) * time.Second,
		MaxActive: 300,
		Password:  "",
		Database:  conf.Conf.Redis.Database,
		Prefix:    "",
		Driver:    redis.GoRedis(), // redis.Radix() can be used instead.
	})
	//fmt.Println(dbForSess)
	app.Use(myMiddleware)
	// rdb.Set("test", "abcfwefwfewfwfe")
	// Close connection when control+C/cmd+C
	iris.RegisterOnInterrupt(func() {
		dbForSess.Close()
	})

	defer dbForSess.Close() // close the database connection if application errored.
	sess := sessions.New(sessions.Config{
		Cookie:       "_session_id",
		Expires:      1440 * 7 * time.Minute, // defaults to 0: unlimited life. Another good value is: 45 * time.Minute,
		AllowReclaim: true,
		//CookieSecureTLS: true,
	})
	sess.UseDatabase(dbForSess)
	app.Use(sess.Handler())
	// session := sessions.Get(ctx)
	// session.Set("udata", "123")
	//aa := rdb.Get("1077")
	//fmt.Println(aa)
	// Get Russian localization context
	lang.Lc = lang.Loc.GetContext("zh-TW")
	// get translation by key name, then format it using Plurr:

	// app.Handle("GET", "/ping", func(ctx iris.Context) {
	// 	ctx.JSON(iris.Map{"message": "pong"})
	// })
	//user.Reg()
	app.Post("/admin/Login", admin.Login)
	app.Post("/admin/CheckLogin", admin.CheckLogin)
	app.Post("/admin/Logout", admin.Logout)
	// app.Post("/task/Analysis", task.Analysis)
	// app.Post("/task/Bonus", task.Bonus)
	// app.Post("/task/Close", task.Close)
	admins := app.Party("/admin", checkSession)
	admins.Post("/AdminAdd", admin.AdminAdd)
	admins.Post("/AdminSet", admin.AdminSet)
	admins.Post("/AdminList", admin.AdminList)

	users := app.Party("/user", checkSession)
	users.Post("/UserList", user.UserList)
	users.Post("/UserSet", user.UserSet)
	users.Post("/UserAdd", user.UserAdd)
	users.Post("/UserBankSet", user.UserBankSet)
	users.Post("/UserBank", user.UserBank)
	users.Post("/UserAddCoin", user.UserAddCoin)

	businesses := app.Party("/business", checkSession)
	businesses.Post("/Transfer/TransferList", business.TransferList)
	businesses.Post("/Recharge/RechargeList", business.RechargeList)
	businesses.Post("/Recharge/RechargeSet", business.RechargeSet)
	businesses.Post("/Cash/CashList", business.CashList)
	businesses.Post("/Cash/CashSet", business.CashSet)
	businesses.Post("/Bid/BidList", business.BidList)
	businesses.Post("/Bid/BidSet", business.BidSet)

	activities := app.Party("/activity", checkSession)
	activities.Post("/BulletinList", activity.BulletinList)
	activities.Post("/BulletinSet", activity.BulletinSet)
	activities.Post("/BulletinAdd", activity.BulletinAdd)

	activities.Post("/PromotionList", activity.PromotionList)
	activities.Post("/PromotionSet", activity.PromotionSet)
	activities.Post("/PromotionAdd", activity.PromotionAdd)

	activities.Post("/NewsList", activity.NewsList)
	activities.Post("/NewsSet", activity.NewsSet)
	activities.Post("/NewsAdd", activity.NewsAdd)

	activities.Post("/VideoList", activity.VideoList)
	activities.Post("/VideoSet", activity.VideoSet)
	activities.Post("/VideoAdd", activity.VideoAdd)

	activities.Post("/MessageList", activity.MessageList)
	activities.Post("/MessageSet", activity.MessageSet)
	activities.Post("/MessageAdd", activity.MessageAdd)
	activities.Post("/MessageInfo", activity.MessageInfo)

	systems := app.Party("/system", checkSession)
	systems.Post("/ParamList", system.ParamList)
	systems.Post("/ParamSet", system.ParamSet)
	systems.Post("/ProdList", system.ProdList)
	systems.Post("/ProdSet", system.ProdSet)
	systems.Post("/ProdSetEnable", system.ProdSetEnable)
	reports := app.Party("/report", checkSession)
	reports.Post("/Dashboard", report.Dashboard)

	fileLimiter := func(ctx iris.Context) {
		if ctx.GetContentLength() > maxSize {
			ctx.StatusCode(iris.StatusRequestEntityTooLarge)
			return
		}
		ctx.Next()
	}
	app.Post("/comm/Upload", fileLimiter, comm.Upload)
	//app.DoneGlobal(after)
	// Listens and serves incoming http requests
	// on http://localhost:8080.
	// config := iris.WithConfiguration(iris.Configuration{
	// 	DisableStartupLog: true,
	// 	//Optimizations:     true,
	// 	Charset: "UTF-8",
	// })
	srv := &http.Server{
		Addr:         ":7676",
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 120 * time.Second,
	}
	srv.SetKeepAlivesEnabled(false)
	app.Run(iris.Server(srv))
	//app.Listen(":7272", config)
}

func checkSession(ctx iris.Context) {
	// ctx.Next()
	// return
	session := sessions.Get(ctx)
	user := session.Get("admin")
	if user == nil {
		ctx.JSON(iris.Map{
			"status": 0,
			"msg":    lang.Lc.Tr("err9"),
			"func":   1,
		})
		return
	}
	permissions := session.Get("admin-permissions")
	//session.Set("admin-permissions", permissions)
	path := strings.Split(ctx.Path(), "/")
	for _, v := range permissions.([]interface{}) {
		val := v.(map[string]interface{})
		chkPath := strings.Split(val["RouterPermissions"].(string), "/")
		if len(chkPath) == 1 {
			continue
		}

		if path[1] == chkPath[1] && chkPath[2] == "*" {
			ctx.Next()
			return
		}

		if path[1] == chkPath[1] && path[2] == chkPath[2] {
			if path[3] == "" {
				ctx.Next()
				return
			}
			if path[3] == chkPath[3] {
				ctx.Next()
				return
			}
			if path[3] != "" && chkPath[3] == "*" {
				ctx.Next()
				return
			}
		}
	}
	//fmt.Println(ctx.Path())
	ctx.JSON(iris.Map{
		"status": 0,
		"msg":    lang.Lc.Tr("admin_err18"),
		"func":   1,
	})
	return
}

func myMiddleware(ctx iris.Context) {
	ctx.Application().Logger().Infof("Runs before %s", ctx.Path())
	ctx.Next()
}

func todayFilename() string {
	today := time.Now().Format("2006-01-02")
	return "./log/" + today + ".txt"
}

func newLogFile() *os.File {
	filename := todayFilename()
	// Open the file, this will append to the today's file if server restarted.
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return f
}
func getenv(key string, def string) string {
	if v := os.Getenv(strings.ToUpper(key)); v != "" {
		return v
	}

	return def
}
