package conf

import (
	"fmt"
	"sync"

	"github.com/BurntSushi/toml"
	"github.com/go-redis/redis/v8"
)

type ConnPool struct {
}

var (
	Rdb      *redis.Client
	instance *ConnPool
	once     sync.Once
	Conf     *Config
)

type Config struct {
	Title    string
	Md5key   string
	Upload   string
	PageSize int
	DB       database  `toml:"database"`
	Redis    RedisConf `toml:"redis"`
	//TransferType map[string]TransferConf `toml:"Transfer"`
}

type TransferConf struct {
	Name string
}

type database struct {
	Source string
}
type RedisConf struct {
	Source   string
	Password string
	Database string
}

func GetInstance() *ConnPool {
	once.Do(func() {
		instance = &ConnPool{}
	})
	return instance
}
func (m *ConnPool) InitConf() {
	var config Config
	if _, err := toml.DecodeFile("../conf/config.toml", &config); err != nil {
		fmt.Println(err)
		return
	}
	Conf = &config
	// Rdb := redis.NewClient(&redis.Options{
	// 	Addr:     config.Redis.Source,
	// 	Password: config.Redis.Password, // no password set
	// 	DB:       0,                     // use default DB
	// })
	// configJSON, err := json.Marshal(config)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// //fmt.Println(string(configJSON))
	// val, err := Rdb.Get("conf").Result()
	// if err == redis.Nil {
	// 	err = Rdb.Set("conf", string(configJSON), 0).Err()
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// } else if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("conf", val)
	// val, err := rdb.Get(ctx, "conf").Result()
	// if err == redis.Nil {
	// 	err := rdb.Set(ctx, "conf", "132", 0).Err()
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// } else if err != nil {
	// 	panic(err)
	// } else {
	// 	fmt.Println("conf", val)
	// }
}
