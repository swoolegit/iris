module db

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	gorm.io/driver/mysql v0.3.1
	gorm.io/gorm v0.2.28
)
