package db

import (
	"database/sql/driver"
	"fmt"
	"log"
	"os"
	"reflect"
	"regexp"
	"sync"
	"time"
	"unicode"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

/* * ConnPool * 數據庫連接操作庫 * 基於gorm封裝開發 */
type ConnPool struct {
}

var (
	instance                 *ConnPool
	once                     sync.Once
	DB                       *gorm.DB
	err_db                   error
	sqlRegexp                = regexp.MustCompile(`\?`)
	numericPlaceHolderRegexp = regexp.MustCompile(`\$\d+`)
	DebugMode                bool
)

type MyLogger struct {
}

func GetInstance() *ConnPool {
	once.Do(func() {
		instance = &ConnPool{}
	})
	return instance
}

/* * @fuc 初始化數據庫連接(可在mail()適當位置調用) */
func (m *ConnPool) InitDataPool(source string) bool {
	//logger := &MyLogger{}
	DebugMode = false
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // Log level 打印所有SQL
			//LogLevel:      logger.Warn, // Log level 印警告以上SQL
			Colorful: true, // 禁用彩色打印
		},
	)
	DB, err_db = gorm.Open(mysql.Open(source), &gorm.Config{
		Logger: newLogger})
	if err_db != nil {
		fmt.Println("failed to connect database:")
		return false
	}
	//DB.DB.SetMaxIdleConns(5)
	//DB := DB.Session(&Session{Logger: newLogger})
	sqlDB, _ := DB.DB()
	sqlDB.SetMaxIdleConns(100)
	sqlDB.SetMaxOpenConns(500)
	sqlDB.SetConnMaxLifetime(time.Hour)
	//DB.LogMode(true)
	//DB.SetLogger(logger)
	//關閉數據庫，db會被多個goroutine共享，可以不調用
	// defer db.Close()
	return true
}

/* * @fuc 對外獲取數據庫連接對象db */
func (m *ConnPool) GetMysqlDB() *gorm.DB {
	return DB
}

func todayFilename() string {
	today := time.Now().Format("2006-01-02")
	return "./log/db/" + today + ".txt"
}

func newLogFile() *os.File {
	filename := todayFilename()
	// Open the file, this will append to the today's file if server restarted.
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return f
}

func isPrintable(s string) bool {
	for _, r := range s {
		if !unicode.IsPrint(r) {
			return false
		}
	}
	return true
}

func (logger *MyLogger) Print(values ...interface{}) {
	logFile := newLogFile()
	loger := log.New(logFile, "[DB] ", log.Ldate|log.Ltime|log.Lshortfile)
	//fmt.Println(values)
	if len(values) > 1 {
		var (
			sql             string
			formattedValues []string
			level           = values[0]
			//currentTime     = "\n\033[33m[" + NowFunc().Format("2006-01-02 15:04:05") + "]\033[0m"
			source = fmt.Sprintf("\033[35m(%v)\033[0m", values[1])
		)
		//messages = []interface{}{source}
		if len(values) == 2 {
			//remove the line break
			//currentTime = currentTime[1:]
			//remove the brackets
			source = fmt.Sprintf("\033[35m%v\033[0m", values[1])
			//messages = []interface{}{currentTime, source}
			loger.Println(source)
		}
		if level == "sql" {
			if !DebugMode {
				return
			}
			// duration
			//messages = append(messages, fmt.Sprintf(" \033[36;1m[%.2fms]\033[0m ", float64(values[2].(time.Duration).Nanoseconds()/1e4)/100.0))
			// sql
			for _, value := range values[4].([]interface{}) {
				indirectValue := reflect.Indirect(reflect.ValueOf(value))
				if indirectValue.IsValid() {
					value = indirectValue.Interface()
					if t, ok := value.(time.Time); ok {
						if t.IsZero() {
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", "0000-00-00 00:00:00"))
						} else {
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", t.Format("2006-01-02 15:04:05")))
						}
					} else if b, ok := value.([]byte); ok {
						if str := string(b); isPrintable(str) {
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", str))
						} else {
							formattedValues = append(formattedValues, "'<binary>'")
						}
					} else if r, ok := value.(driver.Valuer); ok {
						if value, err := r.Value(); err == nil && value != nil {
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
						} else {
							formattedValues = append(formattedValues, "NULL")
						}
					} else {
						switch value.(type) {
						case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, float32, float64, bool:
							formattedValues = append(formattedValues, fmt.Sprintf("%v", value))
						default:
							formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
						}
					}
				} else {
					formattedValues = append(formattedValues, "NULL")
				}
			}
			// differentiate between $n placeholders or else treat like ?
			if numericPlaceHolderRegexp.MatchString(values[3].(string)) {
				sql = values[3].(string)
				for index, value := range formattedValues {
					placeholder := fmt.Sprintf(`\$%d([^\d]|$)`, index+1)
					sql = regexp.MustCompile(placeholder).ReplaceAllString(sql, value+"$1")
				}
			} else {
				formattedValuesLength := len(formattedValues)
				for index, value := range sqlRegexp.Split(values[3].(string), -1) {
					sql += value
					if index < formattedValuesLength {
						sql += formattedValues[index]
					}
				}
			}
			//messages = append(messages, sql)
			loger.Println(sql)
			DebugMode = false
			//messages = append(messages, fmt.Sprintf(" \n\033[36;31m[%v]\033[0m ", strconv.FormatInt(values[5].(int64), 10)+" rows affected or returned "))
		} else {
			//messages = append(messages, "\033[31;1m")
			//messages = append(messages, values[2:]...)
			//messages = append(messages, "\033[0m")
			checkTpye := reflect.TypeOf(values[2]).String()
			if checkTpye == "*mysql.MySQLError" {
				DebugMode = true
				loger.Println(values)
			} else {
				DebugMode = false
			}

		}
	}
}
