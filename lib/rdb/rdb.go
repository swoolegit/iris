package rdb

import (
	"time"

	"github.com/go-redis/redis"
	"iris.wa/conf"
)

var (
	Rdb *redis.Client
)

func Get(key string) interface{} {
	if Rdb == nil {
		Rdb = redis.NewClient(&redis.Options{
			Addr:     conf.Conf.Redis.Source,
			Password: conf.Conf.Redis.Password, // no password set
			DB:       0,                        // use default DB
		})
	}
	val, _ := Rdb.Get(key).Result()
	return val
}
func Set(key string, val interface{}, t int) {
	if Rdb == nil {
		Rdb = redis.NewClient(&redis.Options{
			Addr:     conf.Conf.Redis.Source,
			Password: conf.Conf.Redis.Password, // no password set
			DB:       0,                        // use default DB
		})
	}
	err := Rdb.Set(key, val, time.Duration(t)*time.Second).Err()
	if err != nil {
		panic(err)
	}
}
