module redis

go 1.14

require (
	github.com/go-redis/redis v6.15.8+incompatible
	github.com/kataras/iris/v12 v12.1.8
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
)
