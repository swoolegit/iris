package comm

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"os"
	"reflect"
	"time"

	"github.com/kataras/iris/v12"
	"gorm.io/gorm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/model"
	"iris.wa/rdb"
)

func Md5(str string) string {
	data := []byte(str)
	has := md5.Sum(data)
	md5str := fmt.Sprintf("%x", has)
	return md5str
}
func Random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}
func SetTrans(tx *gorm.DB, setCoin *model.SetCoin) error {
	// (IN `_coin` DECIMAL(20,3), IN `_uid` INT, IN `_type` INT, IN `_extID` INT)
	//tx.Exec("call setCoin("+setCoin.Coin+","+setCoin.UID+","+setCoin.Type+","+setCoin.ExtID+")")
	//if err := tx.Create(&transferLog).Error; err != nil {
	if err := tx.Exec("call setCoin(?,?,?,?)", setCoin.UID, setCoin.Coin, setCoin.Type, setCoin.ExtID).Error; err != nil {
		return err
	}
	return nil
}
func JsonErr(ctx iris.Context, msg string) {
	ctx.JSON(iris.Map{
		"status": 0,
		"msg":    msg,
	})
	ctx.Request().Body.Close()
}

// 有時會傳數組進來,無法實現同時接數組非數組
func Json(ctx iris.Context, data interface{}) {
	ctx.JSON(iris.Map{
		"status": 1,
		"data":   data,
	})
	ctx.Request().Body.Close()
}

type OrderKey struct {
	ID int64
}

func OrderID(db *gorm.DB) int64 {
	var orderKey OrderKey
	db.Raw("call pre_make_key(@orederKey)").Scan(&orderKey)
	return orderKey.ID
}

func SysParams(key string) string {
	var systemMap map[string]string
	systemMap = make(map[string]string)
	params := rdb.Get("system")
	if params == "" {
		var paramsSystems []model.ParamsSystems
		db.DB.Find(&paramsSystems)
		for _, v := range paramsSystems {
			systemMap[v.Key] = v.Value
		}
		jsonBytes, err := json.Marshal(systemMap)
		if err != nil {
			fmt.Println(err)
		}
		rdb.Set("system", jsonBytes, 86400)
	} else {
		json.Unmarshal([]byte(params.(string)), &systemMap)
	}
	return systemMap[key]
}

func InArray(obj interface{}, target interface{}) bool {
	targetValue := reflect.ValueOf(target)
	switch reflect.TypeOf(target).Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < targetValue.Len(); i++ {
			if targetValue.Index(i).Interface() == obj {
				return true
			}
		}
	case reflect.Map:
		if targetValue.MapIndex(reflect.ValueOf(obj)).IsValid() {
			return true
		}
	}
	return false
}
func Upload(ctx iris.Context) {
	// Get the file from the request.
	file, info, err := ctx.FormFile("file")
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		ctx.HTML("Error while uploading: <b>" + err.Error() + "</b>")
		return
	}
	defer file.Close()
	fname := info.Filename
	//创建一个具有相同名称的文件
	//假设你有一个名为'uploads'的文件夹
	out, err := os.OpenFile(conf.Conf.Upload+fname,
		os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		ctx.HTML("Error while uploading: <b>" + err.Error() + "</b>")
		return
	}
	defer out.Close()
	io.Copy(out, file)
	Json(ctx, fname)
	return
}
