package business

import (
	"strconv"

	"github.com/kataras/iris/v12"
	"gorm.io/gorm"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 資金紀錄列表
func TransferList(ctx iris.Context) {
	var page = ctx.FormValue("Page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Username = ctx.FormValue("Username")
	var TransferType = ctx.FormValue("TransferType")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var TransferLogs []model.TransferLog
	var count int64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("transfer_log_repls")
	if FromTime != "" && ToTime != "" {
		DbSQL.Where("created_at >= ? and created_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	}
	if Username != "" {
		DbSQL.Where("username = ?", Username)
	}
	if TransferType != "" {
		DbSQL.Where("Type = ?", TransferType)
	}
	DbSQL.Count(&count)
	DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&TransferLogs)

	var data []map[string]interface{}
	for _, value := range TransferLogs {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Username":  value.Username,
			"Coin":      value.Coin,
			"UserCoin":  value.UserCoin,
			"Type":      value.Type,
			"CreatedAt": value.CreatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"data": data,
	})
	return
}

// 充值紀錄列表
func RechargeList(ctx iris.Context) {
	var page = ctx.FormValue("Page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var RechargeStatus = ctx.FormValue("RechargeStatus")
	var RechargeModel = ctx.FormValue("RechargeModel")
	var RechargeID = ctx.FormValue("RechargeID")
	var Username = ctx.FormValue("Username")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var MemberRecharges []model.MemberRecharges
	var count int64
	var SumTotal float64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("member_recharges")
	db.DB.Table("member_recharges").Select("ifnull(sum(amount),0) as SumTotal").Scopes(ScopeTotal(1), ScopeTime(FromTime, ToTime), ScopeUsername(Username), ScopeStatus(RechargeStatus), ScopeRechargeID(RechargeID), ScopeRechargeModel(RechargeModel)).Scan(&SumTotal)
	DbSQL.Scopes(ScopeTime(FromTime, ToTime), ScopeUsername(Username), ScopeStatus(RechargeStatus), ScopeRechargeID(RechargeID), ScopeRechargeModel(RechargeModel)).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&MemberRecharges)
	DbSQL.Count(&count)

	// DbSQL.Count(&count)
	// DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&MemberRecharges)

	var data []map[string]interface{}
	for _, value := range MemberRecharges {
		data = append(data, map[string]interface{}{
			"ID":                value.ID,
			"UID":               value.UID,
			"Username":          value.Username,
			"RechargeID":        value.RechargeID,
			"RechargeModelName": value.RechargeModelName,
			"RechargeComName":   value.RechargeComName,
			"Status":            value.Status,
			"Info":              value.Info,
			"Memo":              value.Memo,
			"Amount":            value.Amount,
			"CreatedAt":         value.CreatedAt,
			"UpdatedAt":         value.UpdatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"sum":  SumTotal,
		"data": data,
	})
	return
}

// 修改充值狀態
func RechargeSet(ctx iris.Context) {
	var status = ctx.FormValue("Status")
	var id = ctx.FormValue("ID")
	if status == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if id == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	var MemberRecharges model.MemberRecharges
	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").First(&MemberRecharges, "id = ? ", id)
	if MemberRecharges.ID == 0 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if MemberRecharges.Status != 0 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err5"))
		return
	}
	if status == "2" {
		MemberRecharges.Status = 2
	}
	if status == "1" {
		MemberRecharges.Status = 1
		var setCoin model.SetCoin
		setCoin.UID = MemberRecharges.UID
		setCoin.Coin = float64(MemberRecharges.Amount)
		setCoin.Type = 3
		setCoin.ExtID = MemberRecharges.RechargeID
		if err := comm.SetTrans(tx, &setCoin); err != nil {
			tx.Rollback()
			comm.JsonErr(ctx, lang.Lc.Tr("admin_err6"))
			return
		}
	}
	tx.Select("Status").Save(&MemberRecharges)
	tx.Commit()
	comm.Json(ctx, nil)
	return
}

func ScopeTime(FromTime string, ToTime string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if FromTime != "" && ToTime != "" {
			return db.Where("created_at >= ? and created_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
		} else {
			return db.Where("")
		}
	}
}
func ScopeTotal(Status int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("status = ?", Status)
	}
}
func ScopeUsername(Username string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if Username != "" {
			return db.Where("username = ?", Username)
		} else {
			return db.Where("")
		}
	}
}
func ScopeStatus(Status string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		s, _ := strconv.Atoi(Status)
		if Status != "" {
			return db.Where("Status = ?", s)
		} else {
			return db.Where("")
		}
	}
}
func ScopeRechargeID(RechargeID string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if RechargeID != "" {
			return db.Where("recharge_id = ?", RechargeID)
		} else {
			return db.Where("")
		}
	}
}
func ScopeRechargeModel(RechargeModel string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if RechargeModel != "" {
			s, _ := strconv.Atoi(RechargeModel)
			return db.Where("recharge_model = ?", s)
		} else {
			return db.Where("")
		}
	}
}
func ScopeOrderNo(OrderNo string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if OrderNo != "" {
			return db.Where("order_no = ?", OrderNo)
		} else {
			return db.Where("")
		}
	}
}

// 提款紀錄列表
func CashList(ctx iris.Context) {
	var page = ctx.FormValue("Page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Username = ctx.FormValue("Username")
	var CashStatus = ctx.FormValue("CashStatus")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var MemberCashs []model.MemberCashs
	var count int64
	var SumTotal float64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("member_cashs")
	// if FromTime != "" && ToTime != "" {
	// 	DbSQL.Where("created_at >= ? and created_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	// 	//SQL.append(SQL,"created_at >=  and created_at <= ?")
	// }
	// if Username != "" {
	// 	DbSQL.Where("username = ?", Username)
	// }
	// if CashStatus != "" {
	// 	DbSQL.Where("Status = ?", CashStatus)
	// }
	//SumSQL.Select("ifnull(sum(amount),0) as SumTotal").Where("status = 1").Scan(&SumTotal)
	db.DB.Table("member_cashs").Select("ifnull(sum(amount),0) as SumTotal").Scopes(ScopeTotal(1), ScopeTime(FromTime, ToTime), ScopeUsername(Username), ScopeStatus(CashStatus)).Scan(&SumTotal)
	DbSQL.Scopes(ScopeTime(FromTime, ToTime), ScopeUsername(Username), ScopeStatus(CashStatus)).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&MemberCashs)
	DbSQL.Count(&count)

	var data []map[string]interface{}
	for _, value := range MemberCashs {
		data = append(data, map[string]interface{}{
			"ID":          value.ID,
			"UID":         value.UID,
			"Username":    value.Username,
			"Status":      value.Status,
			"Amount":      value.Amount,
			"BankID":      value.BankID,
			"BankName":    value.BankName,
			"Account":     value.Account,
			"Accountname": value.Accountname,
			"Info":        value.Info,
			"UpdatedAt":   value.UpdatedAt,
			"CreatedAt":   value.CreatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"sum":  SumTotal,
		"data": data,
	})
	return
}

// 修改提款狀態
func CashSet(ctx iris.Context) {
	var status = ctx.FormValue("Status")
	var id = ctx.FormValue("ID")
	var Info = ctx.FormValue("Info")
	if status == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if id == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	var MemberCashs model.MemberCashs
	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").First(&MemberCashs, "id = ? ", id)

	if MemberCashs.ID == 0 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if MemberCashs.Status != 0 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err5"))
		return
	}
	if status == "2" {
		MemberCashs.Status = 2
		var setCoin model.SetCoin
		setCoin.UID = MemberCashs.UID
		setCoin.Coin = float64(MemberCashs.Amount)
		setCoin.Type = 5
		setCoin.ExtID = int64(MemberCashs.ID)
		if err := comm.SetTrans(tx, &setCoin); err != nil {
			tx.Rollback()
			comm.JsonErr(ctx, lang.Lc.Tr("admin_err6"))
			return
		}
	}
	if status == "1" {
		MemberCashs.Status = 1
	}
	MemberCashs.Info = Info
	tx.Select("Status", "Info").Save(&MemberCashs)
	tx.Commit()
	comm.Json(ctx, nil)
	return
}

// 下標紀錄列表
func BidList(ctx iris.Context) {
	var page = ctx.FormValue("Page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Username = ctx.FormValue("Username")
	var BidStatus = ctx.FormValue("BidStatus")
	var OrderNo = ctx.FormValue("OrderNo")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var BidLogs []model.BidLogs
	var SumTotal float64
	var count int64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("bid_log_repls")
	db.DB.Table("bid_log_repls").Select("ifnull(sum(coin),0) as SumTotal").Scopes(ScopeTime(FromTime, ToTime), ScopeUsername(Username), ScopeStatus(BidStatus), ScopeOrderNo(OrderNo)).Scan(&SumTotal)
	DbSQL.Scopes(ScopeTime(FromTime, ToTime), ScopeUsername(Username), ScopeStatus(BidStatus), ScopeOrderNo(OrderNo)).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&BidLogs)
	DbSQL.Count(&count)

	var data []map[string]interface{}
	for _, value := range BidLogs {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"OrderNo":   value.OrderNo,
			"UID":       value.UID,
			"Username":  value.Username,
			"Status":    value.Status,
			"Coin":      value.Coin,
			"UserCoin":  value.UserCoin,
			"ProdName":  value.ProdName,
			"Winer":     value.Winer,
			"EndTime":   value.EndTime,
			"CreatedAt": value.CreatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"sum":  SumTotal,
		"data": data,
	})
	return
}

// 修改下單狀態
func BidSet(ctx iris.Context) {
	var id = ctx.FormValue("ID")
	if id == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	var BidLogs model.BidLogs
	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").First(&BidLogs, "id = ? ", id)
	if BidLogs.Status != 0 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err5"))
		return
	}
	if BidLogs.Winer == 0 {
		BidLogs.Winer = 1
	} else {
		BidLogs.Winer = 0
	}
	tx.Select("Winer").Save(&BidLogs)
	tx.Commit()
	comm.Json(ctx, nil)
	return
}
