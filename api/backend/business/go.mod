module business

go 1.14

require (
	github.com/kataras/iris v0.0.2
	github.com/kataras/iris/v12 v12.1.8
	gorm.io/gorm v1.20.7
)
