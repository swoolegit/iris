package admin

import (
	"strconv"
	"strings"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 列表
func AdminList(ctx iris.Context) {
	var AdminMembers []model.AdminMembers
	db.DB.Find(&AdminMembers)
	comm.Json(ctx, AdminMembers)
	return
}

// 登錄
func Login(ctx iris.Context) {
	var username = ctx.FormValue("Username")
	var password = ctx.FormValue("Password")
	if username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}
	if password == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err2"))
		return
	}
	var AdminMembers model.AdminMembers
	db.DB.First(&AdminMembers, "Username = ?", username)
	if AdminMembers.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if AdminMembers.Password != comm.Md5(password+conf.Conf.Md5key) {
		comm.JsonErr(ctx, lang.Lc.Tr("err3"))
		return
	}
	if AdminMembers.Enable == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err10"))
		return
	}
	var AdminGroups model.AdminGroups
	db.DB.First(&AdminGroups, "ID = ?", AdminMembers.GroupID)
	var AdminRouters []model.AdminRouters
	db.DB.Where("router_type in ?", strings.Split(AdminGroups.GroupPermissions, ",")).Select("RouterType", "RouterName", "RouterPath", "RouterPermissions").Find(&AdminRouters)
	var routerPermissions []map[string]interface{}
	routers := make(map[int][]map[string]interface{})
	var routerData map[string]interface{}
	for _, v := range AdminRouters {
		routerData = map[string]interface{}{
			"RouterType":        v.RouterType,
			"RouterName":        v.RouterName,
			"RouterPath":        v.RouterPath,
			"RouterPermissions": v.RouterPermissions,
		}
		routers[v.RouterType] = append(routers[v.RouterType], routerData)
		routerPermissions = append(routerPermissions, routerData)
	}
	session := sessions.Get(ctx)
	session.Set("admin", AdminMembers)
	session.Set("admin-router", routers)
	session.Set("admin-permissions", routerPermissions)
	// session := sessions.Get(ctx)
	// session.Set("uid", members.ID)
	AdminMembers.LoginAt = time.Now()
	AdminMembers.LoginIP = ctx.RemoteAddr()
	db.DB.Select("LoginAt", "LoginIP").Save(&AdminMembers)
	comm.Json(ctx, map[string]interface{}{
		"ID":       AdminMembers.ID,
		"Username": AdminMembers.Username,
		"GroupID":  AdminMembers.GroupID,
		"Router":   routers,
	})
	return
}

// 登出
func Logout(ctx iris.Context) {
	session := sessions.Get(ctx)
	session.Delete("admin")
	session.Delete("admin-router")
	session.Delete("admin-permissions")
	comm.Json(ctx, nil)
	return
}

// 登錄
func CheckLogin(ctx iris.Context) {
	session := sessions.Get(ctx)
	userChk := session.Get("admin")
	if userChk == nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	user := session.Get("admin").(map[string]interface{})
	//.(map[string]interface{})

	var AdminMembers model.AdminMembers
	db.DB.First(&AdminMembers, "id = ?", int(user["ID"].(float64)))
	if AdminMembers.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if AdminMembers.Enable == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err10"))
		return
	}
	//session.Set("user", AdminMembers)
	routers := session.Get("admin-router").(map[string]interface{})
	comm.Json(ctx, map[string]interface{}{
		"ID":       AdminMembers.ID,
		"Username": AdminMembers.Username,
		"GroupID":  AdminMembers.GroupID,
		"Router":   routers,
	})
	return
}

// 建帳號
func AdminAdd(ctx iris.Context) {
	var username = ctx.FormValue("Username")
	var password = ctx.FormValue("Password")
	var groupID = ctx.FormValue("GroupID")
	var Enable = ctx.FormValue("Enable")
	var memo = ctx.FormValue("Memo")

	if username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}
	if password == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err2"))
		return
	}
	if groupID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err1"))
		return
	}
	gid, _ := strconv.Atoi(groupID)
	var AdminMembers model.AdminMembers

	AdminMembers.Username = username
	AdminMembers.Password = comm.Md5(password + conf.Conf.Md5key)
	AdminMembers.GroupID = gid
	AdminMembers.Enable, _ = strconv.Atoi(Enable)
	AdminMembers.Memo = memo

	if err := db.DB.Create(&AdminMembers).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err2"))
		return
	}

	comm.Json(ctx, map[string]interface{}{
		"ID": AdminMembers.ID,
	})
	return
}

// 改密碼
func AdminSet(ctx iris.Context) {
	var username = ctx.FormValue("Username")
	var password = ctx.FormValue("Password")
	var GroupID = ctx.FormValue("GroupID")
	var Enable = ctx.FormValue("Enable")
	var Memo = ctx.FormValue("Memo")
	if username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}
	var AdminMembers model.AdminMembers
	db.DB.First(&AdminMembers, "Username = ?", username)
	if AdminMembers.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if password != "" {
		AdminMembers.Password = comm.Md5(password + conf.Conf.Md5key)
	}
	AdminMembers.GroupID, _ = strconv.Atoi(GroupID)
	AdminMembers.Enable, _ = strconv.Atoi(Enable)
	AdminMembers.Memo = Memo
	db.DB.Select("Password", "GroupID", "Enable", "Memo").Save(&AdminMembers)
	comm.Json(ctx, nil)
	return
}
