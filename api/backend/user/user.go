package user

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

type code struct {
	base    string // 进制的包含字符, string类型
	decimal uint64 // 进制长度
	pad     string // 补位字符,若生成的code小于最小长度,则补位+随机字符, 补位字符不能在进制字符中
	len     int    // code最小长度
}

// id转code
func (c *code) idToCode(id uint64) string {
	mod := uint64(0)
	res := ""
	for id != 0 {
		mod = id % c.decimal
		id = id / c.decimal
		res += string(c.base[mod])
	}
	resLen := len(res)
	if resLen < c.len {
		res += c.pad
		for i := 0; i < c.len-resLen-1; i++ {
			rand.Seed(time.Now().UnixNano())
			res += string(c.base[rand.Intn(int(c.decimal))])
		}
	}
	return res
}

// code转id
func (c *code) codeToId(code string) uint64 {
	res := uint64(0)
	lenCode := len(code)
	//var baseArr [] byte = []byte(c.base)
	baseArr := []byte(c.base)     // 字符串进制转换为byte数组
	baseRev := make(map[byte]int) // 进制数据键值转换为map
	for k, v := range baseArr {
		baseRev[v] = k
	}

	// 查找补位字符的位置
	isPad := strings.Index(code, c.pad)
	if isPad != -1 {
		lenCode = isPad
	}

	r := 0
	for i := 0; i < lenCode; i++ {
		// 补充字符直接跳过
		if string(code[i]) == c.pad {
			continue
		}
		index := baseRev[code[i]]
		b := uint64(1)
		for j := 0; j < r; j++ {
			b *= c.decimal
		}
		// pow 类型为 float64 , 类型转换太麻烦, 所以自己循环实现pow的功能
		//res += float64(index) * math.Pow(float64(32), float64(2))
		res += uint64(index) * b
		r++
	}
	return res
}

// 初始化检查
func (c *code) initCheck() (bool, error) {
	lenBase := len(c.base)
	// 检查进制字符
	if c.base == "" {
		return false, errors.New("base string is nil or empty")
	}
	// 检查长度是否符合
	if uint64(lenBase) != c.decimal {
		return false, errors.New("base length and len not match")
	}
	return true, errors.New("")
}

func getCode() code {
	return code{
		base:    "HVE8S2DZX9C7P5IK3MJUAR4WYLTN6BGQ",
		decimal: 32,
		pad:     "F",
		len:     6,
	}
}

// 列表
func UserList(ctx iris.Context) {
	var page = ctx.FormValue("page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Username = ctx.FormValue("Username")
	var LoginIP = ctx.FormValue("LoginIP")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	var members []model.Members
	var count int64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("members")
	if FromTime != "" && ToTime != "" {
		DbSQL.Where("login_at >= ? and login_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	}
	if Username != "" {
		DbSQL.Where("username like ?", "%"+Username+"%")
	}
	if LoginIP != "" {
		DbSQL.Where("login_ip = ?", LoginIP)
	}
	DbSQL.Count(&count)
	DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&members)

	var data []map[string]interface{}
	for _, value := range members {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Username":  value.Username,
			"Enable":    value.Enable,
			"Coin":      value.Coin,
			"LoginIP":   value.LoginIP,
			"RegIP":     value.RegIP,
			"Phone":     value.Phone,
			"Line":      value.Line,
			"Email":     value.Email,
			"LoginAt":   value.LoginAt,
			"CreatedAt": value.CreatedAt,
			"Memo":      value.Memo,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	// fmt.Println(int64(conf.Conf.PageSize))
	// fmt.Println(diffPage)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"data": data,
	})
	return
}

// 列表
func UserBank(ctx iris.Context) {
	var Username = ctx.FormValue("Username")
	if Username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}

	var MemberBanks model.MemberBanks
	db.DB.First(&MemberBanks, "Username = ?", Username)
	data := map[string]interface{}{
		"ID":          MemberBanks.ID,
		"Username":    MemberBanks.Username,
		"BankID":      MemberBanks.BankID,
		"BankName":    MemberBanks.BankName,
		"Account":     MemberBanks.Account,
		"Accountname": MemberBanks.Accountname,
	}

	var BankLists []model.BankLists
	db.DB.Where("for_cash = ? and enable = ?", 1, 1).Find(&BankLists)
	comm.Json(ctx, map[string]interface{}{
		"data": data,
		"bank": BankLists,
	})
	return
}

// 修改用戶訊息
func UserSet(ctx iris.Context) {
	var Username = ctx.FormValue("Username")
	var Password = ctx.FormValue("Password")
	var CoinPassword = ctx.FormValue("CoinPassword")
	var Memo = ctx.FormValue("Memo")
	var Phone = ctx.FormValue("Phone")
	var Line = ctx.FormValue("Line")
	var Email = ctx.FormValue("Email")
	var Enable = ctx.FormValue("Enable")

	if Username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}

	var members model.Members
	db.DB.First(&members, "Username = ?", Username)
	if Password != "" {
		members.Password = comm.Md5(Password + conf.Conf.Md5key)
	}
	members.Password = comm.Md5(Password + conf.Conf.Md5key)
	if CoinPassword != "" {
		members.CoinPassword = comm.Md5(CoinPassword + conf.Conf.Md5key)
	}
	members.Memo = Memo
	members.Phone = Phone
	members.Line = Line
	members.Email = Email
	members.Enable, _ = strconv.Atoi(Enable)
	db.DB.Save(&members)
	comm.Json(ctx, nil)
	return
}

// 修改用戶訊息
func UserAddCoin(ctx iris.Context) {
	var Username = ctx.FormValue("Username")
	var Amount = ctx.FormValue("Amount")
	var Type = ctx.FormValue("Type")
	if Username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}
	AddCoin, err1 := strconv.ParseFloat(Amount, 64)
	if err1 != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err27"))
		return
	}
	AddType, _ := strconv.Atoi(Type)
	NameData := strings.Split(Username, ",")
	var memberRecharges model.MemberRecharges
	var setCoin model.SetCoin
	if AddType == 1 {
		setCoin.Type = 3
	}
	if AddType == 2 {
		setCoin.Type = 6
	}
	if AddType == 3 {
		setCoin.Type = 8
		AddCoin = AddCoin * -1
	}
	var Members []model.Members
	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").Where("username IN ? ", NameData).Find(&Members)
	for _, value := range Members {
		setCoin.ExtID = 0
		if AddType == 1 {
			ID := comm.OrderID(db.DB)
			memberRecharges.UID = value.ID
			memberRecharges.Username = value.Username
			memberRecharges.RechargeID = ID
			memberRecharges.ActionIP = ctx.RemoteAddr()
			memberRecharges.Amount = AddCoin
			memberRecharges.RechargeModel = 2 // 管理員充值
			memberRecharges.RechargeComID = 0
			memberRecharges.RechargeComName = lang.Lc.Tr("msg3")
			memberRecharges.RechargeModelName = lang.Lc.Tr("msg3")
			if err := db.DB.Create(&memberRecharges).Error; err != nil {
				tx.Rollback()
				comm.JsonErr(ctx, lang.Lc.Tr("err31"))
				return
			}
			setCoin.ExtID = int64(memberRecharges.ID)
			memberRecharges.ID = 0
		}
		setCoin.Coin = AddCoin
		setCoin.UID = value.ID

		if err := comm.SetTrans(tx, &setCoin); err != nil {
			tx.Rollback()
			comm.JsonErr(ctx, lang.Lc.Tr("err31"))
			return
		}
	}
	tx.Commit()
	comm.Json(ctx, nil)
	return
}

// 修改用戶訊息
func UserAdd(ctx iris.Context) {
	var Username = ctx.FormValue("Username")
	var Password = ctx.FormValue("Password")
	var CoinPassword = ctx.FormValue("CoinPassword")
	var Memo = ctx.FormValue("Memo")
	var Phone = ctx.FormValue("Phone")
	var Line = ctx.FormValue("Line")
	var Email = ctx.FormValue("Email")
	var Enable = ctx.FormValue("Enable")

	if Username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}
	var members model.Members
	members.Username = Username
	//db.DB.First(&members, "Username = ?", Username)
	if Password != "" {
		members.Password = comm.Md5(Password + conf.Conf.Md5key)
	}
	members.Password = comm.Md5(Password + conf.Conf.Md5key)
	if CoinPassword != "" {
		members.CoinPassword = comm.Md5(CoinPassword + conf.Conf.Md5key)
	}
	members.Memo = Memo
	members.Phone = Phone
	members.Line = Line
	members.Email = Email
	members.Enable, _ = strconv.Atoi(Enable)
	//db.DB.Save(&members)
	if err := db.DB.Create(&members).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err10"))
		return
	}
	inviteCode := getCode()
	if res, err := inviteCode.initCheck(); !res {
		fmt.Println(err)
		return
	}
	var inviteLinks2 model.InviteLinks
	inviteLinks2.UID = members.ID
	inviteLinks2.Enable = 1
	inviteLinks2.Code = inviteCode.idToCode(uint64(members.ID))
	inviteLinks2.Type = 1 //預設代理
	db.DB.Create(&inviteLinks2)

	comm.Json(ctx, nil)
	return
}

// 修改用戶訊息
func UserBankSet(ctx iris.Context) {
	var Username = ctx.FormValue("Username")
	var BankID = ctx.FormValue("BankID")
	var Account = ctx.FormValue("Account")
	var Accountname = ctx.FormValue("Accountname")

	if Username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if BankID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err22"))
		return
	}
	var Members model.Members
	db.DB.First(&Members, "Username = ?", Username)

	var BankLists model.BankLists
	db.DB.Select("Name,id").First(&BankLists, "id = ?", BankID)
	type Temp struct {
		BankID      int
		Username    string
		Account     string
		BankName    string
		Accountname string
		Enable      int
		UID         int
	}
	var MemberBanks model.MemberBanks
	db.DB.Where(Temp{Username: Username}).Assign(Temp{BankID: BankLists.ID, BankName: BankLists.Name, Account: Account, Accountname: Accountname, Enable: 1, UID: Members.ID}).FirstOrCreate(&MemberBanks)
	//db.DB.First(&MemberBanks, "username = ?", Username)

	// MemberBanks.BankID = BankLists.ID
	// MemberBanks.BankName = BankLists.Name
	// MemberBanks.Account = Account
	// MemberBanks.Accountname = Accountname
	// db.DB.Save(&MemberBanks)
	comm.Json(ctx, nil)
	return
}
