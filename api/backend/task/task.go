package task

import (
	"fmt"
	"strconv"
	"time"

	"iris.wa/comm"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

//func Analysis(ctx iris.Context) {
func Analysis() {
	chkTime := time.Now().AddDate(0, 0, -1)
	t1 := chkTime.Format("2006-01-02")
	var RegUser int
	db.DB.Raw("select ifnull(count(ID),0) from members where created_at >= ? and created_at <= ?", t1+" 00:00:00", t1+" 23:59:59").Scan(&RegUser)
	var LoginUser int
	db.DB.Raw("select ifnull(count(ID),0) from members where login_at >= ? and login_at <= ?", t1+" 00:00:00", t1+" 23:59:59").Scan(&LoginUser)
	var RechargeUser int
	db.DB.Raw("select ifnull(count(DISTINCT uid),0) from member_recharges where updated_at >= ? and updated_at <= ? and status = 1 ", t1+" 00:00:00", t1+" 23:59:59").Scan(&RechargeUser)
	var BidUser int
	db.DB.Raw("select ifnull(count(DISTINCT uid),0) as BidUser from member_recharges where created_at >= ? and created_at <= ? and status = 1", t1+" 00:00:00", t1+" 23:59:59").Scan(&BidUser)
	type MemberReport struct {
		BidAmount      float64
		BonusAmount    float64
		RebateAmount   float64
		RechargeAmount float64
		CashAmount     float64
		BrokerAmount   float64
	}
	var res MemberReport
	db.DB.Raw("select ifnull(sum(recharge),0) as RechargeAmount,ifnull(sum(cash),0) as CashAmount,ifnull(sum(rebate),0) as RebateAmount,ifnull(sum(bonus),0) as BonusAmount,ifnull(sum(bid),0) as BidAmount,ifnull(sum(broker),0) as BrokerAmount from member_reports where actionDate >= ? and actionDate <= ? ", t1+" 00:00:00", t1+" 23:59:59").Scan(&res)
	var ReportDays model.ReportDays
	ReportDays.RegUser = RegUser
	ReportDays.RechargeUser = RechargeUser
	ReportDays.LoginUser = LoginUser
	ReportDays.BidUser = BidUser
	ReportDays.BidAmount = res.BidAmount
	ReportDays.BonusAmount = res.BonusAmount
	ReportDays.RebateAmount = res.RebateAmount
	ReportDays.RechargeAmount = res.RechargeAmount
	ReportDays.CashAmount = res.CashAmount
	ReportDays.BrokerAmount = res.BrokerAmount
	ReportDays.Date = chkTime
	if err := db.DB.Create(&ReportDays).Error; err != nil {
		//comm.JsonErr(ctx, lang.Lc.Tr("admin_err2"))
		fmt.Println(lang.Lc.Tr("admin_err2"))
		return
	}
}

// 以下要放到後台執行
// 結標
//func Close(ctx iris.Context) {
func Close() {
	// hello := lang.Lc.Format("YouHaveNMessages", plurr.Params{"N": 5})
	// fmt.Println(hello)
	var bidlog []model.BidLogs
	//var bidlog2 []model.BidLogs
	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").Where("status = ? AND end_time <= ?", 0, time.Now()).Find(&bidlog)
	if len(bidlog) < 1 {
		// ctx.JSON(iris.Map{
		// 	"status": 1,
		// })
		fmt.Println("status : 1")
		return
	}
	key := comm.Random(0, len(bidlog))
	// begin 處理rebate
	agentID := make(map[int]map[string]interface{})
	var agentKey []int
	rebate1, _ := strconv.ParseFloat(comm.SysParams("rebate1"), 64)
	rebate2, _ := strconv.ParseFloat(comm.SysParams("rebate2"), 64)
	rebate3, _ := strconv.ParseFloat(comm.SysParams("rebate3"), 64)
	for k, v := range bidlog {
		if len(agentID[v.ParentID]) > 0 {
			agentID[v.ParentID]["rebate1"] = agentID[v.ParentID]["rebate1"].(float64) + (v.Coin*rebate1)/100
		} else {
			agentID[v.ParentID] = map[string]interface{}{
				//"id":      v.ParentID,
				"rebate1": (v.Coin * rebate1) / 100,
			}
		}
		if len(agentID[v.Parent2ID]) > 0 {
			agentID[v.Parent2ID]["rebate1"] = agentID[v.Parent2ID]["rebate1"].(float64) + (v.Coin*rebate2)/100
		} else {
			agentID[v.Parent2ID] = map[string]interface{}{
				//"id":      v.ParentID,
				"rebate1": (v.Coin * rebate2) / 100,
			}
		}
		if len(agentID[v.Parent3ID]) > 0 {
			agentID[v.Parent3ID]["rebate1"] = agentID[v.Parent3ID]["rebate1"].(float64) + (v.Coin*rebate3)/100
		} else {
			agentID[v.Parent3ID] = map[string]interface{}{
				//"id":      v.ParentID,
				"rebate1": (v.Coin * rebate3) / 100,
			}
		}
		if !comm.InArray(v.ParentID, agentKey) && v.ParentID > 0 {
			agentKey = append(agentKey, v.ParentID)
		}
		// fmt.Println(v)
		// fmt.Println(v.Winer, k, key)
		if v.Winer == 1 && k != key {
			key = k
		}
	}
	// for k, v := range bidlog {
	// 	if len(agentID[v.ParentID]) > 0 {
	// 		agentID[v.ParentID]["rebate1"] = agentID[v.ParentID]["rebate1"].(float64) + (v.Coin*rebate1)/100
	// 		agentID[v.ParentID]["rebate2"] = agentID[v.ParentID]["rebate2"].(float64) + (v.Coin*rebate2)/100
	// 		agentID[v.ParentID]["rebate3"] = agentID[v.ParentID]["rebate3"].(float64) + (v.Coin*rebate3)/100
	// 	} else {
	// 		agentID[v.ParentID] = map[string]interface{}{
	// 			"id":      v.ParentID,
	// 			"rebate1": (v.Coin * rebate1) / 100,
	// 			"rebate2": (v.Coin * rebate2) / 100,
	// 			"rebate3": (v.Coin * rebate3) / 100,
	// 		}
	// 	}
	// 	if !comm.InArray(v.ParentID, agentKey) && v.ParentID > 0 {
	// 		agentKey = append(agentKey, v.ParentID)
	// 	}
	// 	// fmt.Println(v)
	// 	// fmt.Println(v.Winer, k, key)
	// 	if v.Winer == 1 && k != key {
	// 		key = k
	// 	}
	// }
	// var members []model.Members
	// tx.Select("id", "parent_id","parents").Where("id in (?)", agentKey).Find(&members)
	// for _, v := range members {
	// 	if v.ParentID == 0 {
	// 		continue
	// 	}
	// 	if len(agentID[v.ParentID]) > 0 {
	// 		agentID[v.ParentID]["rebate1"] = agentID[v.ParentID]["rebate1"].(float64) + agentID[v.ID]["rebate2"].(float64)
	// 	} else {
	// 		agentID[v.ParentID] = map[string]interface{}{
	// 			"rebate1": agentID[v.ID]["rebate2"],
	// 			"rebate2": agentID[v.ID]["rebate3"],
	// 		}
	// 	}
	// }

	var preBonus []model.PreBonus
	var preBonusTmp model.PreBonus
	for k, value := range agentID {
		if k > 0 {
			preBonusTmp.UID = k
			preBonusTmp.Coin = value["rebate1"].(float64)
			preBonusTmp.Type = 7 // 反點是7
			preBonusTmp.ExtID = 0
			preBonus = append(preBonus, preBonusTmp)
		}
	}
	if len(preBonus) > 0 {
		//tx.Delete(PreBonus{})
		tx.Exec("delete from pre_bonus")
		if err := tx.Create(&preBonus).Error; err != nil {
			tx.Rollback()
			//comm.JsonErr(ctx, err.Error())
			fmt.Println(err.Error())
			return
		}
		if err := tx.Exec("call preBonus();").Error; err != nil {
			tx.Rollback()
			//comm.JsonErr(ctx, err.Error())
			fmt.Println(err.Error())
			return
		}
	}
	// end 處理rebate

	if err := tx.Model(&model.BidLogs{}).Where("id = ?", bidlog[key].ID).Update("status", 1).Error; err != nil {
		tx.Rollback()
		//comm.JsonErr(ctx, err.Error())
		fmt.Println(err.Error())
		return
	}

	var product model.Products
	tx.First(&product, "id = ?", bidlog[key].ProdID)

	var memberProds model.MemberProds
	now := time.Now()
	memberProds.UID = bidlog[key].UID
	memberProds.ProdID = product.ID
	memberProds.Name = product.Name
	memberProds.MaxPrice = product.MaxPrice
	memberProds.BonusDay = now.AddDate(0, 0, product.BonusDay+1)
	memberProds.Bonus = product.Bonus
	if err := tx.Create(&memberProds).Error; err != nil {
		tx.Rollback()
		//comm.JsonErr(ctx, lang.Lc.Tr("err11"))
		fmt.Println(lang.Lc.Tr("err11"))
		return
	}
	if len(bidlog) > 1 {
		bidlog = append(bidlog[:key], bidlog[key+1:]...)
		var updateKey []int
		for _, v := range bidlog {
			updateKey = append(updateKey, v.ID)
		}
		if err := tx.Model(&model.BidLogs{}).Where("id in (?)", updateKey).Update("status", 2).Error; err != nil {
			tx.Rollback()
			//comm.JsonErr(ctx, err.Error())
			fmt.Println(err.Error())
			return
		}
	}
	//tx.Rollback()
	tx.Commit()

	// ctx.JSON(iris.Map{
	// 	"status": 1,
	// })

	return
	// for rows.Next(){
	// 	rows.Scan(&name, &age, &email) ...
	// }
}

// 發放利息
//func Bonus(ctx iris.Context) {
func Bonus() {
	var memberProds []model.MemberProds
	var Products model.Products
	var preBonus []model.PreBonus
	var preBonusTmp model.PreBonus
	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").Where("status = ? AND bonus_day <= ?", 0, time.Now()).Find(&memberProds)
	var key []int
	var updatePrice float64
	for _, value := range memberProds {
		key = append(key, value.ID)
		preBonusTmp.UID = value.UID
		preBonusTmp.Coin = value.Price * (value.Bonus / 100)
		preBonusTmp.Type = 2 // 利息的type為2
		preBonusTmp.ExtID = value.ID
		preBonus = append(preBonus, preBonusTmp)
		updatePrice = value.Price + value.Price*(value.Bonus/100)
		if updatePrice > value.MaxPrice {
			updatePrice = value.MaxPrice
		}
		if err := tx.Model(&Products).Where("id = ?", value.ProdID).Update("price", updatePrice).Error; err != nil {
			tx.Rollback()
			//comm.JsonErr(ctx, err.Error())
			fmt.Println(err.Error())
			return
		}
	}
	if len(preBonus) > 0 {
		if err := tx.Create(&preBonus).Error; err != nil {
			tx.Rollback()
			//comm.JsonErr(ctx, err.Error())
			fmt.Println(err.Error())
			return
		}
		if err := tx.Exec("call preBonus();").Error; err != nil {
			tx.Rollback()
			//comm.JsonErr(ctx, err.Error())
			fmt.Println(err.Error())
			return
		}
	}
	if len(key) > 0 {
		if err := tx.Model(&memberProds).Where("id IN (?)", key).Update("status", 1).Error; err != nil {
			tx.Rollback()
			//comm.JsonErr(ctx, err.Error())
			fmt.Println(err.Error())
			return
		}
	}
	fmt.Println("Bonus")
	tx.Commit()
	//comm.Json(ctx, nil)
	return
}
