package activity

import (
	"strconv"

	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 資金紀錄列表
func PromotionList(ctx iris.Context) {
	var page = ctx.FormValue("page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Title = ctx.FormValue("Title")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var Promotions []model.Promotions
	var count int64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("promotions")
	if FromTime != "" && ToTime != "" {
		DbSQL.Where("created_at >= ? and created_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	}
	if Title != "" {
		DbSQL.Where("title like ?", "%"+Title+"%")
	}
	DbSQL.Count(&count)
	DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&Promotions)

	var data []map[string]interface{}
	for _, value := range Promotions {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Title":     value.Title,
			"Content":   value.Content,
			"Img":       value.Img,
			"Enable":    value.Enable,
			"CreatedAt": value.CreatedAt,
			"UpdatedAt": value.UpdatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"data": data,
	})
	return
}

// 資金紀錄列表
func PromotionSet(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	var Enable = ctx.FormValue("Enable")
	var Title = ctx.FormValue("Title")
	var Content = ctx.FormValue("Content")
	var Img = ctx.FormValue("Img")

	if Enable == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Content == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}
	if Img == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err9"))
		return
	}
	var Promotions model.Promotions
	db.DB.First(&Promotions, "id = ?", ID)
	Promotions.Title = Title
	Promotions.Content = Content
	Promotions.Enable, _ = strconv.Atoi(Enable)
	Promotions.Img = Img
	db.DB.Save(&Promotions)
	comm.Json(ctx, nil)
	return
}

// 資金紀錄列表
func PromotionAdd(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	var Enable = ctx.FormValue("Enable")
	var Title = ctx.FormValue("Title")
	var Content = ctx.FormValue("Content")
	var Img = ctx.FormValue("Img")

	if Enable == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Content == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}
	if Img == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err9"))
		return
	}
	var Promotions model.Promotions
	Promotions.Title = Title
	Promotions.Content = Content
	Promotions.Enable, _ = strconv.Atoi(Enable)
	Promotions.Img = Img
	if err := db.DB.Create(&Promotions).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err10"))
		return
	}
	comm.Json(ctx, nil)
	return
}
