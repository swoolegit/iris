package activity

import (
	"strconv"

	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 資金紀錄列表
func VideoList(ctx iris.Context) {
	var page = ctx.FormValue("page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Title = ctx.FormValue("Title")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var Video []model.Videos
	var count int64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("videos")
	if FromTime != "" && ToTime != "" {
		DbSQL.Where("created_at >= ? and created_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	}
	if Title != "" {
		DbSQL.Where("title like ?", "%"+Title+"%")
	}
	DbSQL.Count(&count)
	DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&Video)

	var data []map[string]interface{}
	for _, value := range Video {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Title":     value.Title,
			"Url":       value.Url,
			"Enable":    value.Enable,
			"CreatedAt": value.CreatedAt,
			"UpdatedAt": value.UpdatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"data": data,
	})
	return
}

// 資金紀錄列表
func VideoSet(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	var Enable = ctx.FormValue("Enable")
	var Title = ctx.FormValue("Title")
	var Url = ctx.FormValue("Url")

	if Enable == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Url == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}

	var Video model.Videos
	db.DB.First(&Video, "id = ?", ID)
	Video.Title = Title
	Video.Url = Url
	Video.Enable, _ = strconv.Atoi(Enable)
	db.DB.Save(&Video)
	comm.Json(ctx, nil)
	return
}

// 資金紀錄列表
func VideoAdd(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	var Enable = ctx.FormValue("Enable")
	var Title = ctx.FormValue("Title")
	var Url = ctx.FormValue("Url")

	if Enable == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Url == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}

	var Video model.Videos
	Video.Title = Title
	Video.Url = Url
	Video.Enable, _ = strconv.Atoi(Enable)
	if err := db.DB.Create(&Video).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err10"))
		return
	}
	comm.Json(ctx, nil)
	return
}
