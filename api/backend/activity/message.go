package activity

import (
	"strconv"
	"strings"

	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 資金紀錄列表
func MessageList(ctx iris.Context) {
	var page = ctx.FormValue("page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Title = ctx.FormValue("Title")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var MessageSenders []model.MessageSenders
	var count int64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("message_senders")
	if FromTime != "" && ToTime != "" {
		DbSQL.Where("created_at >= ? and created_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	}
	if Title != "" {
		DbSQL.Where("title like ?", "%"+Title+"%")
	}
	DbSQL.Count(&count)
	DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&MessageSenders)

	//db.DB.Model(&MessageSenders).Count(&count)
	//db.DB.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&MessageSenders)

	var data []map[string]interface{}
	for _, value := range MessageSenders {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Username":  value.Username,
			"Title":     value.Title,
			"Content":   value.Content,
			"CreatedAt": value.CreatedAt,
			"UpdatedAt": value.UpdatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"data": data,
	})
	return
}

// 資金紀錄列表
func MessageSet(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	var Title = ctx.FormValue("Title")
	var Content = ctx.FormValue("Content")
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Content == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}
	var MessageSenders model.MessageSenders
	db.DB.First(&MessageSenders, "id = ?", ID)
	MessageSenders.Title = Title
	MessageSenders.Content = Content
	MessageSenders.Username = "Admin"
	MessageSenders.UID = 0
	db.DB.Save(&MessageSenders)
	comm.Json(ctx, nil)
	return
}

// 資金紀錄列表
func MessageInfo(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	var MessageReceivers []model.MessageReceivers

	db.DB.Select("username").Find(&MessageReceivers, "message_id = ?", ID)
	var data []map[string]interface{}
	for _, value := range MessageReceivers {
		data = append(data, map[string]interface{}{
			"Username": value.Username,
		})
	}
	comm.Json(ctx, data)
	return
}

// 資金紀錄列表
func MessageAdd(ctx iris.Context) {
	var Title = ctx.FormValue("Title")
	var Content = ctx.FormValue("Content")
	var List = ctx.FormValue("List")
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Content == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}
	var MessageSenders model.MessageSenders
	MessageSenders.Title = Title
	MessageSenders.Content = Content
	MessageSenders.Username = "Admin"
	MessageSenders.UID = 0

	if err := db.DB.Create(&MessageSenders).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err10"))
		return
	}
	var Members []model.Members
	if List == "" {
		db.DB.Select("ID", "Username").Find(&Members)
	} else {
		username := strings.Split(List, ",")
		db.DB.Select("ID", "Username").Where("Username in ?", username).Find(&Members)
	}
	var MessageReceivers []model.MessageReceivers
	var Tmp model.MessageReceivers
	for _, v := range Members {
		Tmp.MessageID = MessageSenders.ID
		Tmp.UID = v.ID
		Tmp.Username = v.Username
		Tmp.IsReaded = 0
		MessageReceivers = append(MessageReceivers, Tmp)
	}
	db.DB.Create(&MessageReceivers)
	comm.Json(ctx, nil)
	return
}
