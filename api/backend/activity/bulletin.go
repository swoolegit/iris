package activity

import (
	"strconv"

	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 資金紀錄列表
func BulletinList(ctx iris.Context) {
	var page = ctx.FormValue("Page")
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var Title = ctx.FormValue("Title")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var Bulletins []model.Bulletins
	var count int64
	DbSQL := db.DB
	DbSQL = DbSQL.Table("bulletins")
	if FromTime != "" && ToTime != "" {
		DbSQL.Where("created_at >= ? and created_at <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	}
	if Title != "" {
		DbSQL.Where("title like ?", "%"+Title+"%")
	}
	DbSQL.Count(&count)
	DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&Bulletins)

	var data []map[string]interface{}
	for _, value := range Bulletins {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Enable":    value.Enable,
			"Title":     value.Title,
			"Content":   value.Content,
			"CreatedAt": value.CreatedAt,
			"UpdatedAt": value.UpdatedAt,
		})
	}
	var totalPage int64
	diffPage := count % int64(conf.Conf.PageSize)
	if diffPage > 0 {
		totalPage = (count / int64(conf.Conf.PageSize)) + 1
	} else {
		totalPage = diffPage
	}
	comm.Json(ctx, map[string]interface{}{
		"page": map[string]interface{}{
			"page":      pageInt,
			"pageSize":  conf.Conf.PageSize,
			"totalPage": totalPage,
			"total":     count,
		},
		"data": data,
	})
	return
}

// 資金紀錄列表
func BulletinSet(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	var Enable = ctx.FormValue("Enable")
	var Title = ctx.FormValue("Title")
	var Content = ctx.FormValue("Content")

	if Enable == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Content == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}
	var Bulletins model.Bulletins
	db.DB.First(&Bulletins, "id = ?", ID)
	Bulletins.Title = Title
	Bulletins.Content = Content
	Bulletins.Enable, _ = strconv.Atoi(Enable)
	db.DB.Save(&Bulletins)
	comm.Json(ctx, nil)
	return
}

// 資金紀錄列表
func BulletinAdd(ctx iris.Context) {
	var ID = ctx.FormValue("ID")
	var Enable = ctx.FormValue("Enable")
	var Title = ctx.FormValue("Title")
	var Content = ctx.FormValue("Content")

	if Enable == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err3"))
		return
	}
	if ID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err4"))
		return
	}
	if Title == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err7"))
		return
	}
	if Content == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err8"))
		return
	}
	var Bulletins model.Bulletins
	db.DB.First(&Bulletins, "id = ?", ID)
	Bulletins.Title = Title
	Bulletins.Content = Content
	Bulletins.Enable, _ = strconv.Atoi(Enable)
	//db.DB.Save(&Bulletins)
	if err := db.DB.Create(&Bulletins).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("admin_err2"))
		return
	}
	comm.Json(ctx, nil)
	return
}
