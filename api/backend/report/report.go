package report

import (
	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/db"
	"iris.wa/model"
)

// 資金紀錄列表
func Dashboard(ctx iris.Context) {
	var FromTime = ctx.FormValue("FromTime")
	var ToTime = ctx.FormValue("ToTime")
	var ReportDays []model.ReportDays
	DbSQL := db.DB
	DbSQL = DbSQL.Table("report_days")
	if FromTime != "" && ToTime != "" {
		DbSQL.Where("date >= ? and date <= ?", FromTime+" 00:00:00", ToTime+" 23:59:59")
	}
	DbSQL.Find(&ReportDays)
	var SumData map[string]interface{}
	var RegUser int
	var RechargeUser int
	var LoginUser int
	var BidUser int
	var BidAmount float64
	var BonusAmount float64
	var RebateAmount float64
	var RechargeAmount float64
	var CashAmount float64
	var BrokerAmount float64
	for _, value := range ReportDays {
		RegUser = RegUser + value.RegUser
		RechargeUser = RechargeUser + value.RechargeUser
		LoginUser = LoginUser + value.LoginUser
		BidUser = BidUser + value.BidUser
		BidAmount = BidAmount + value.BidAmount
		BonusAmount = BonusAmount + value.BonusAmount
		RebateAmount = RebateAmount + value.RebateAmount
		RechargeAmount = RechargeAmount + value.RechargeAmount
		CashAmount = CashAmount + value.CashAmount
		BrokerAmount = BrokerAmount + value.BrokerAmount
	}
	SumData = map[string]interface{}{
		"RegUser":        RegUser,
		"RechargeUser":   RechargeUser,
		"LoginUser":      LoginUser,
		"BidUser":        BidUser,
		"BidAmount":      BidAmount,
		"BonusAmount":    BonusAmount,
		"RebateAmount":   RebateAmount,
		"RechargeAmount": RechargeAmount,
		"CashAmount":     CashAmount,
		"BrokerAmount":   BrokerAmount,
	}
	comm.Json(ctx, map[string]interface{}{
		"SumData":   SumData,
		"ReportDay": ReportDays,
	})
	return
}
