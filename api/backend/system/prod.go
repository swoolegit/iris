package system

import (
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/iafan/Plurr/go/plurr"
	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 資金紀錄列表
func ProdList(ctx iris.Context) {
	var Products []model.Products
	db.DB.Find(&Products)

	var data []map[string]interface{}
	for _, value := range Products {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Name":      value.Name,
			"Price":     value.Price,
			"MaxPrice":  value.MaxPrice,
			"BonusDay":  value.BonusDay,
			"Bonus":     value.Bonus,
			"Cost":      value.Cost,
			"Enable":    value.Enable,
			"Ending":    value.Ending,
			"Note":      value.Note,
			"CreatedAt": value.CreatedAt,
			"UpdatedAt": value.UpdatedAt,
		})
	}
	comm.Json(ctx, data)
	return
}
func ParseFormCollection(r *http.Request, typeName string) []map[string]string {
	var result []map[string]string
	r.ParseForm()
	for key, values := range r.Form {
		re := regexp.MustCompile(typeName + "\\[([0-9]+)\\]\\[([a-zA-Z]+)\\]")
		matches := re.FindStringSubmatch(key)

		if len(matches) >= 3 {

			index, _ := strconv.Atoi(matches[1])

			for index >= len(result) {
				result = append(result, map[string]string{})
			}

			result[index][matches[2]] = values[0]
		}
	}
	return result
}

// 資金紀錄列表
func ProdSet(ctx iris.Context) {
	var Key = ctx.FormValues()
	data := make(map[string]map[string]interface{})
	len1 := 0
	len2 := 0
	for k, v := range Key {
		len1 = strings.Index(k, "[")
		len2 = strings.Index(k, "]")
		if k[len1+1:len2] != "Note" {
			if v[0] == "" {
				comm.JsonErr(ctx, lang.Lc.Format("admin_err19", plurr.Params{"N": k[len1+1 : len2]}))
				return
			}
		}
		if len(data[k[0:len1]]) > 0 {
			data[k[0:len1]][k[len1+1:len2]] = v[0]
		} else {
			data[k[0:len1]] = map[string]interface{}{
				k[len1+1 : len2]: v[0],
			}
		}
	}
	var Products model.Products
	var err error
	for _, v := range data {
		Products.Name = v["Name"].(string)
		Products.Price, err = strconv.ParseFloat(v["Price"].(string), 64)
		Products.Enable, err = strconv.Atoi(v["Enable"].(string))
		Products.MaxPrice, err = strconv.ParseFloat(v["MaxPrice"].(string), 64)
		Products.BonusDay, err = strconv.Atoi(v["BonusDay"].(string))
		Products.Bonus, err = strconv.ParseFloat(v["Bonus"].(string), 64)
		Products.Cost, err = strconv.ParseFloat(v["Cost"].(string), 64)
		Products.Ending = v["Ending"].(string)
		Products.Note = v["Note"].(string)
		if err != nil {
			comm.JsonErr(ctx, err.Error())
			return
		}
		db.DB.Model(&Products).Where("id = ?", v["ID"]).Updates(Products)
	}
	comm.Json(ctx, nil)
	return
}

func ProdSetEnable(ctx iris.Context) {
	var Enable = ctx.FormValue("Enable")
	var ID = ctx.FormValue("ID")
	var Products model.Products
	db.DB.First(&Products, "id = ?", ID)
	var err error
	Products.Enable, err = strconv.Atoi(Enable)
	if err != nil {
		comm.JsonErr(ctx, err.Error())
		return
	}
	db.DB.Save(&Products)
	comm.Json(ctx, nil)
	return
}
