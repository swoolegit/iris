module system

go 1.14

require (
	github.com/iafan/Plurr v0.0.0-20200416004027-ca96e77a8f76
	github.com/kataras/iris/v12 v12.1.8
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
)
