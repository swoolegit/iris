package system

import (
	"encoding/json"

	"github.com/kataras/iris/v12"
	"iris.wa/comm"
	"iris.wa/db"
	"iris.wa/model"
	"iris.wa/rdb"
)

// 資金紀錄列表
func ParamList(ctx iris.Context) {
	var ParamsSystems []model.ParamsSystems
	db.DB.Find(&ParamsSystems)

	data := make(map[string]string)
	for _, v := range ParamsSystems {
		data[v.Key] = v.Value
	}
	comm.Json(ctx, data)
	return
}

// 資金紀錄列表
func ParamSet(ctx iris.Context) {
	var Key = ctx.FormValues()
	db.DB.Exec("delete from params_systems")
	var ParamsSystems []model.ParamsSystems
	var Tmp model.ParamsSystems
	var systemMap map[string]string
	systemMap = make(map[string]string)
	for k, v := range Key {
		Tmp.Key = k
		Tmp.Value = v[0]
		ParamsSystems = append(ParamsSystems, Tmp)
		systemMap[k] = v[0]
	}
	db.DB.Create(&ParamsSystems)
	jsonBytes, _ := json.Marshal(systemMap)
	rdb.Set("system", jsonBytes, 86400)
	comm.Json(ctx, nil)
	return
}
