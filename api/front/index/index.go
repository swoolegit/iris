package index

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

type code struct {
	base    string // 进制的包含字符, string类型
	decimal uint64 // 进制长度
	pad     string // 补位字符,若生成的code小于最小长度,则补位+随机字符, 补位字符不能在进制字符中
	len     int    // code最小长度
}

// id转code
func (c *code) idToCode(id uint64) string {
	mod := uint64(0)
	res := ""
	for id != 0 {
		mod = id % c.decimal
		id = id / c.decimal
		res += string(c.base[mod])
	}
	resLen := len(res)
	if resLen < c.len {
		res += c.pad
		for i := 0; i < c.len-resLen-1; i++ {
			rand.Seed(time.Now().UnixNano())
			res += string(c.base[rand.Intn(int(c.decimal))])
		}
	}
	return res
}

// code转id
func (c *code) codeToId(code string) uint64 {
	res := uint64(0)
	lenCode := len(code)
	//var baseArr [] byte = []byte(c.base)
	baseArr := []byte(c.base)     // 字符串进制转换为byte数组
	baseRev := make(map[byte]int) // 进制数据键值转换为map
	for k, v := range baseArr {
		baseRev[v] = k
	}

	// 查找补位字符的位置
	isPad := strings.Index(code, c.pad)
	if isPad != -1 {
		lenCode = isPad
	}

	r := 0
	for i := 0; i < lenCode; i++ {
		// 补充字符直接跳过
		if string(code[i]) == c.pad {
			continue
		}
		index := baseRev[code[i]]
		b := uint64(1)
		for j := 0; j < r; j++ {
			b *= c.decimal
		}
		// pow 类型为 float64 , 类型转换太麻烦, 所以自己循环实现pow的功能
		//res += float64(index) * math.Pow(float64(32), float64(2))
		res += uint64(index) * b
		r++
	}
	return res
}

// 初始化检查
func (c *code) initCheck() (bool, error) {
	lenBase := len(c.base)
	// 检查进制字符
	if c.base == "" {
		return false, errors.New("base string is nil or empty")
	}
	// 检查长度是否符合
	if uint64(lenBase) != c.decimal {
		return false, errors.New("base length and len not match")
	}
	return true, errors.New("")
}

func getCode() code {
	return code{
		base:    "HVE8S2DZX9C7P5IK3MJUAR4WYLTN6BGQ",
		decimal: 32,
		pad:     "F",
		len:     6,
	}
}

// 銀行列表
func Bank(ctx iris.Context) {
	var bankLists []model.BankLists
	db.DB.Find(&bankLists, "enable = 1 and for_cash=1 and area = 'tw'")
	comm.Json(ctx, bankLists)
	return

}

// 公告
func Bulletin(ctx iris.Context) {
	var page = ctx.FormValue("page")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var bulletins []model.Bulletins
	db.DB.Where("enable = 1").Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&bulletins)
	comm.Json(ctx, bulletins)
	return
}

// 優惠活動
func Promotion(ctx iris.Context) {
	var promotions []model.Promotions
	db.DB.Find(&promotions, "enable = 1")
	comm.Json(ctx, promotions)
	return
}

// 公告
func News(ctx iris.Context) {
	var page = ctx.FormValue("page")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var News []model.News
	db.DB.Where("enable = 1").Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&News)
	comm.Json(ctx, News)
	return
}

// 視頻代碼
func VideoList(ctx iris.Context) {
	var page = ctx.FormValue("page")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	var Video []model.Videos
	db.DB.Where("enable = 1").Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&Video)
	comm.Json(ctx, Video)
	return
}

// 最後一筆視頻代碼
func VideoLast(ctx iris.Context) {
	var Video []model.Videos
	db.DB.Where("enable = 1").Order("created_at desc").Limit(1).Find(&Video)
	comm.Json(ctx, Video)
	return
}

// Reg註冊
func Reg(ctx iris.Context) {
	var members model.Members
	//dbconn := db.GetInstance().GetMysqlDB()
	//db.Db.First(&members, "Username = ?", "eegg20")
	//fmt.Println(members)
	var username = ctx.FormValue("Username")
	var password = ctx.FormValue("Password")
	var code = ctx.FormValue("Code")
	var phone = ctx.FormValue("Phone")
	if username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}
	if password == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err2"))
		return
	}
	var inviteLinks model.InviteLinks
	var inviteMembers model.Members
	if code != "" {
		if err := db.DB.First(&inviteLinks, "code = ?", code).Error; err != nil {
			comm.JsonErr(ctx, lang.Lc.Tr("err13"))
			return
		}
		db.DB.First(&inviteMembers, "id = ?", inviteLinks.UID)
	}
	members.Username = username
	members.Password = comm.Md5(password + conf.Conf.Md5key)
	members.Phone = phone
	members.Enable = 1
	members.RegIP = ctx.RemoteAddr()
	members.LoginIP = ctx.RemoteAddr()
	members.LoginAt = time.Now()

	if err := db.DB.Create(&members).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err4"))
		return
	}
	if inviteMembers.ID > 0 {
		db.DB.Model(&members).Updates(model.Members{ParentID: inviteMembers.ID, Parents: inviteMembers.Parents + "," + strconv.Itoa(members.ID)})
		//db.DB.Model(&members).Updates("parents", inviteMembers.Parents+","+strconv.Itoa(members.ID))
		db.DB.Model(&inviteLinks).Update("used", inviteLinks.Used+1)
		// inviteLinks.Used = inviteLinks.Used + 1
		// db.DB.Save(&inviteLinks)
	} else {
		db.DB.Model(&members).Update("parents", members.ID)
	}

	inviteCode := getCode()
	if res, err := inviteCode.initCheck(); !res {
		fmt.Println(err)
		return
	}
	var inviteLinks2 model.InviteLinks
	inviteLinks2.UID = members.ID
	inviteLinks2.Enable = 1
	inviteLinks2.Code = inviteCode.idToCode(uint64(members.ID))
	inviteLinks2.Type = 1 //預設代理
	db.DB.Create(&inviteLinks2)

	session := sessions.Get(ctx)
	session.Set("user", members)
	comm.Json(ctx, map[string]interface{}{
		"ID":       members.ID,
		"Username": members.Username,
		"Coin":     members.Coin,
	})
	return
}

// 登錄
func Login(ctx iris.Context) {
	// hello := lang.Lc.Format("YouHaveNMessages", plurr.Params{"N": 5})
	// fmt.Println(hello)
	// inviteCode := getCode()

	// // 初始化检查
	// if res, err := inviteCode.initCheck(); !res {
	// 	fmt.Println(err)
	// 	return
	// }
	// id := uint64(1000)

	// code := inviteCode.idToCode(id)

	// fmt.Printf("id=%v, code=%v\n", id, code)

	// code = "TVVFWW"
	// id = inviteCode.codeToId(code)
	// fmt.Printf("code=%v, id=%v\n", code, id)

	var username = ctx.FormValue("Username")
	var password = ctx.FormValue("Password")
	if username == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err1"))
		return
	}
	if password == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err2"))
		return
	}
	var members model.Members
	db.DB.First(&members, "Username = ?", username)
	if members.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if members.Password != comm.Md5(password+conf.Conf.Md5key) {
		comm.JsonErr(ctx, lang.Lc.Tr("err3"))
		return
	}
	if members.Enable == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err10"))
		return
	}
	//rdb.Set(strconv.Itoa(members.ID), toJSON, 300)
	session := sessions.Get(ctx)
	session.Set("user", members)
	// session := sessions.Get(ctx)
	// session.Set("uid", members.ID)
	members.LoginAt = time.Now()
	members.LoginIP = ctx.RemoteAddr()
	db.DB.Select("LoginAt", "LoginIP").Save(&members)
	//db.DB.Model(&members).Where("id = ?", members.ID).Update(map[string]interface{}{"login_ip": ctx.RemoteAddr(), LoginAt: time.now()})
	comm.Json(ctx, map[string]interface{}{
		"ID":       members.ID,
		"Username": members.Username,
		"Coin":     members.Coin,
	})
	return
}
func Logout(ctx iris.Context) {
	session := sessions.Get(ctx)
	session.Delete("user")
	comm.Json(ctx, nil)
	return
}
