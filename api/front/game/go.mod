module game

go 1.14

require (
	github.com/goinggo/mapstructure v0.0.0-20140717182941-194205d9b4a9
	github.com/kataras/iris/v12 v12.1.8
	github.com/satori/go.uuid v1.2.0
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
)
