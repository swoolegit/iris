package game

import (
	"strconv"
	"strings"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	uuid "github.com/satori/go.uuid"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// type Members struct {
// 	ID           int
// 	ParentID     int
// 	Enable       int
// 	FanDian      float64
// 	Username     string
// 	Password     string
// 	CoinPassword string
// 	Memo         string
// 	Phone        string
// 	Coin         float64
// 	CreatedAt    time.Time
// 	UpdatedAt    time.Time
// 	DeletedAt    *time.Time
// }

// 產品列表
func Prod(ctx iris.Context) {
	var product []model.Products
	var page = ctx.FormValue("page")
	if page == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	table := "products"
	db.DB.Table(table).Where("enable = ?", 1).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Scan(&product)
	// for _, v := range bidlog {
	// 	fmt.Println(v)
	// }
	comm.Json(ctx, product)
	return

}

// 競標
func Bid(ctx iris.Context) {
	var product model.Products
	var pid = ctx.FormValue("Pid")
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})

	db.DB.First(&product, "id = ?", pid)
	if product.ID < 1 {
		comm.JsonErr(ctx, lang.Lc.Tr("err5"))
		return
	}
	//now := time.Now()
	//now.Format("2006/01/02")
	t1, err := time.ParseInLocation("2006-01-02 15:04:05", time.Now().Format("2006-01-02")+" "+product.Ending, time.Local)
	t2 := time.Now()
	if err == nil && t1.Before(t2) {
		comm.JsonErr(ctx, lang.Lc.Tr("err6"))
		return
	}
	//session := sessions.Get(ctx)
	//user := (session.Get("user")).(model.Members)
	//var u model.Members // 不指定反序列化的类型
	//user := session.Get("user").(map[string]interface{})
	//mapstructure.Decode(user, &u) // map 轉 struct
	var members model.Members
	var bidlog model.BidLogs
	//fmt.Println("type:", reflect.TypeOf(user["ID"]))
	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").First(&members, "id = ? AND enable = ?", int(user["ID"].(float64)), 1)
	//fmt.Println(members)
	if members.ID < 1 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if members.Coin < product.Cost {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err8"))
		return
	}
	Parry := strings.Split(members.Parents, ",")
	if len(Parry) == 3 {
		Parent2ID, _ := strconv.Atoi(Parry[0])
		bidlog.Parent2ID = Parent2ID
		bidlog.Parent3ID = 0
	}
	if len(Parry) > 3 {
		Parent2ID, _ := strconv.Atoi(Parry[len(Parry)-3])
		Parent3ID, _ := strconv.Atoi(Parry[len(Parry)-4])
		bidlog.Parent2ID = Parent2ID
		bidlog.Parent3ID = Parent3ID
	}
	orderNo := uuid.NewV4()
	bidlog.UID = members.ID
	bidlog.OrderNo = orderNo.String()
	bidlog.Username = members.Username
	bidlog.ProdID = product.ID
	bidlog.ProdName = product.Name
	bidlog.Coin = product.Cost
	bidlog.UserCoin = members.Coin - product.Cost
	bidlog.EndTime = t1
	bidlog.ParentID = members.ParentID
	if err := tx.Create(&bidlog).Error; err != nil {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err32"))
		return
	}
	var setCoin model.SetCoin
	setCoin.UID = members.ID
	setCoin.Coin = product.Cost * -1
	setCoin.Type = 1
	setCoin.ExtID = int64(bidlog.ID)
	if err := comm.SetTrans(tx, &setCoin); err != nil {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err14"))
		return
	}
	//tx.Model(&members).Update("coin", bidlog.UserCoin)
	//tx..First(&members, "id = ? AND enable = ?", u.ID, 1)
	tx.Commit()
	comm.Json(ctx, nil)
	return
}
