package user

import (
	"fmt"
	"strconv"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	"gorm.io/gorm"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

// 檢查用戶狀態
func Safe(ctx iris.Context) {
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})

	var members model.Members
	uid := int(user["ID"].(float64))
	db.DB.First(&members, "id = ?", uid)
	if members.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if members.Enable == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err10"))
		return
	}
	session.Set("user", members)

	safe := map[string]interface{}{
		"ID":           members.ID,
		"BankID":       0,
		"Username":     members.Username,
		"Phone":        "",
		"Email":        members.Email,
		"CoinPassword": false,
		"BankName":     "",
		"Account":      "",
		"Accountname":  "",
	}

	if members.Phone != "" {
		safe["Phone"] = members.Phone[:len(members.Phone)-5] + "*****"
	}
	var memberBanks model.MemberBanks
	db.DB.First(&memberBanks, "uid = ? and enable = 1", uid)
	if memberBanks.ID > 0 {
		safe["BankName"] = memberBanks.BankName
		safe["Username"] = memberBanks.Username
		safe["Account"] = memberBanks.Account[:len(memberBanks.Account)-4] + "****"
		safe["Accountname"] = memberBanks.Accountname
		safe["BankID"] = memberBanks.BankID
	}
	if members.CoinPassword != "" {
		safe["CoinPassword"] = true
	}
	comm.Json(ctx, safe)
	return
}

// 檢查用戶狀態
func SafeUp(ctx iris.Context) {
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))
	var _type = ctx.FormValue("type")
	switch _type {
	case "1":
		var password = ctx.FormValue("password")
		var newPassword = ctx.FormValue("newPassword")
		if len(password) < 4 || len(newPassword) < 4 {
			comm.JsonErr(ctx, lang.Lc.Tr("err15"))
			return
		}
		var members model.Members
		db.DB.First(&members, "id = ?", uid)
		if comm.Md5(password+conf.Conf.Md5key) != members.Password {
			comm.JsonErr(ctx, lang.Lc.Tr("err16"))
			return
		}
		db.DB.Model(&members).Where("id = ?", uid).Update("Password", comm.Md5(newPassword+conf.Conf.Md5key))
		comm.Json(ctx, nil)
		return
	case "2":
		var coinpassword = ctx.FormValue("coinpassword")
		var confirmCoinpassword = ctx.FormValue("confirmCoinpassword")
		if len(coinpassword) < 4 || len(confirmCoinpassword) < 4 {
			comm.JsonErr(ctx, lang.Lc.Tr("err15"))
			return
		}
		var members model.Members
		db.DB.First(&members, "id = ?", uid)
		if members.CoinPassword == "" {
			db.DB.Model(&members).Where("id = ?", uid).Update("coin_password", comm.Md5(confirmCoinpassword+conf.Conf.Md5key))
			comm.Json(ctx, nil)
			return
		}
		if members.CoinPassword != comm.Md5(coinpassword+conf.Conf.Md5key) {
			comm.JsonErr(ctx, lang.Lc.Tr("err16"))
			return
		}
		db.DB.Model(&members).Where("id = ?", uid).Update("coin_password", comm.Md5(confirmCoinpassword+conf.Conf.Md5key))
		comm.Json(ctx, nil)
		return
	case "3":
		var phone = ctx.FormValue("phone")
		if phone == "" {
			comm.JsonErr(ctx, lang.Lc.Tr("err17"))
			return
		}
		var members model.Members
		db.DB.First(&members, "id = ?", uid)
		if members.Phone != "" {
			comm.JsonErr(ctx, lang.Lc.Tr("err18"))
			return
		}
		db.DB.Model(&members).Where("id = ?", uid).Update("phone", phone)
		comm.Json(ctx, nil)
		return
	case "4":
		var email = ctx.FormValue("email")
		if email == "" {
			comm.JsonErr(ctx, lang.Lc.Tr("err19"))
			return
		}
		var members model.Members
		db.DB.Model(&members).Where("id = ?", uid).Update("email", email)
		comm.Json(ctx, nil)
		return
	case "5":
		var username = ctx.FormValue("username")
		var account = ctx.FormValue("account")
		var bank = ctx.FormValue("bank")
		if username == "" {
			comm.JsonErr(ctx, lang.Lc.Tr("err20"))
			return
		}
		if account == "" {
			comm.JsonErr(ctx, lang.Lc.Tr("err21"))
			return
		}
		if bank == "" {
			comm.JsonErr(ctx, lang.Lc.Tr("err22"))
			return
		}
		var memberBanks model.MemberBanks
		db.DB.First(&memberBanks, "uid = ? and enable = 1", uid)
		if memberBanks.ID > 0 {
			comm.JsonErr(ctx, lang.Lc.Tr("err23"))
			return
		}
		db.DB.First(&memberBanks, "account = ? and enable = 1", account)
		if memberBanks.ID > 0 {
			comm.JsonErr(ctx, lang.Lc.Tr("err24"))
			return
		}
		var bankLists model.BankLists
		db.DB.First(&bankLists, "id = ? and enable = 1 and for_cash=1 and area = 'tw'", bank)
		if bankLists.ID == 0 {
			comm.JsonErr(ctx, lang.Lc.Tr("err22"))
			return
		}
		memberBanks.UID = uid
		memberBanks.BankID = bankLists.ID
		memberBanks.BankName = bankLists.Name
		memberBanks.Username = user["Username"].(string)
		memberBanks.Account = account
		memberBanks.Accountname = username
		memberBanks.Enable = 1

		if err := db.DB.Create(&memberBanks).Error; err != nil {
			comm.JsonErr(ctx, lang.Lc.Tr("err25"))
			return
		}
		comm.Json(ctx, nil)
		return
	}
}

// 檢查登入
func CheckLogin(ctx iris.Context) {
	session := sessions.Get(ctx)
	userChk := session.Get("user")
	if userChk == nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	user := session.Get("user").(map[string]interface{})

	// tx := db.DB.Begin()
	// tx.Set("gorm:query_option", "FOR UPDATE").First(&members, "id = ? AND enable = ?", user["ID"], 1)
	var members model.Members
	db.DB.First(&members, "id = ?", int(user["ID"].(float64)))
	if members.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if members.Enable == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err10"))
		return
	}
	session.Set("user", members)
	comm.Json(ctx, map[string]interface{}{
		"ID":       members.ID,
		"Username": members.Username,
		"Coin":     members.Coin,
	})
	return
}

func BidLog(ctx iris.Context) {
	var bidlog []model.BidLogs
	var page = ctx.FormValue("page")
	var status = ctx.FormValue("status")
	var fromTime = ctx.FormValue("fromTime")
	var toTime = ctx.FormValue("toTime")
	if page == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	statusInt, err := strconv.Atoi(status)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	//db.DB.Where("uid = ?", int(uid.(float64))).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&bidlog)
	table := "bid_logs_tmp" + strconv.Itoa(uid%10)
	Db := db.DB
	Db = Db.Table(table).Where("uid = ? and created_at >= ? and created_at <= ?", uid, fromTime+" 00:00:00", toTime+" 23:59:59")
	if statusInt < 4 {
		Db = Db.Table(table).Where("status = ?", statusInt)
	}
	Db.Table(table).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Scan(&bidlog)
	// for _, v := range bidlog {
	// 	fmt.Println(v)
	// }
	comm.Json(ctx, bidlog)
	return
}

// RechargeInfo
func RechargeInfo(ctx iris.Context) {
	var paramsBanks []model.ParamsBanks
	db.DB.Find(&paramsBanks, "enable = 1")
	var paramsThirdpays model.ParamsThirdpays
	db.DB.Find(&paramsThirdpays, "payin_status = 1")
	var thirdBankLists []model.BankLists
	switch paramsThirdpays.ID {
	case 1:
		db.DB.Select("id", "name", "ename as code").Find(&thirdBankLists, "enable = 1 and for_recharge=1 and area = ?", paramsThirdpays.Area)
	}
	comm.Json(ctx, iris.Map{
		"bank":     paramsBanks,
		"third":    thirdBankLists,
		"third_id": paramsThirdpays.ID,
	})
	return
}

// 提款
func Cash(ctx iris.Context) {
	var amount = ctx.FormValue("amount")
	var password = ctx.FormValue("password")
	amountInt, err := strconv.Atoi(amount)
	if err != nil || amountInt <= 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err27"))
		return
	}
	if password == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err2"))
		return
	}
	cashMax, _ := strconv.Atoi(comm.SysParams("cashMax"))
	cashMin, _ := strconv.Atoi(comm.SysParams("cashMin"))
	if amountInt > cashMax {
		comm.JsonErr(ctx, lang.Lc.Tr("err30"))
		return
	}
	if amountInt < cashMin {
		comm.JsonErr(ctx, lang.Lc.Tr("err29"))
		return
	}
	password = comm.Md5(password + conf.Conf.Md5key)

	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	var members model.Members
	uid := int(user["ID"].(float64))

	var memberBanks model.MemberBanks
	db.DB.First(&memberBanks, "uid = ? and enable = 1", uid)
	if memberBanks.ID < 1 {
		comm.JsonErr(ctx, lang.Lc.Tr("err33"))
		return
	}

	tx := db.DB.Begin()
	tx.Set("gorm:query_option", "FOR UPDATE").First(&members, "id = ? AND enable = ?", uid, 1)
	if members.ID < 1 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	if members.Coin < float64(amountInt) {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err8"))
		return
	}
	if members.Enable == 0 {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err10"))
		return
	}
	if members.CoinPassword != password {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err3"))
		return
	}
	var memberCashs model.MemberCashs
	memberCashs.UID = uid
	memberCashs.Username = user["Username"].(string)
	memberCashs.Amount = amountInt
	memberCashs.BankID = memberBanks.BankID
	memberCashs.BankName = memberBanks.BankName
	memberCashs.Account = memberBanks.Account
	memberCashs.Accountname = memberBanks.Accountname
	memberCashs.Status = 0
	if err := tx.Create(&memberCashs).Error; err != nil {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err32"))
		return
	}
	var setCoin model.SetCoin
	setCoin.UID = uid
	setCoin.Coin = float64(amountInt * -1)
	setCoin.Type = 4
	setCoin.ExtID = int64(memberCashs.ID)
	if err := comm.SetTrans(tx, &setCoin); err != nil {
		tx.Rollback()
		comm.JsonErr(ctx, lang.Lc.Tr("err34"))
		return
	}
	tx.Commit()
	comm.Json(ctx, nil)
	return
}

// RechargeBank
func RechargeBank(ctx iris.Context) {
	var memo = ctx.FormValue("memo")
	var amount = ctx.FormValue("amount")
	var bankid = ctx.FormValue("bankid")
	//var rechargeType = ctx.FormValue("rechargeType")
	if memo == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err26"))
		return
	}
	bankidInt, _ := strconv.Atoi(bankid)
	amountInt, err := strconv.Atoi(amount)
	if err != nil || amountInt <= 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err27"))
		return
	}
	var paramsBanks model.ParamsBanks
	db.DB.First(&paramsBanks, "enable = 1 and id = ?", bankid)
	if paramsBanks.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err28"))
		return
	}
	if amountInt < paramsBanks.MinAmount {
		comm.JsonErr(ctx, lang.Lc.Tr("err29"))
		return
	}
	if amountInt > paramsBanks.MaxAmount {
		comm.JsonErr(ctx, lang.Lc.Tr("err30"))
		return
	}
	ID := comm.OrderID(db.DB)
	var memberRecharges model.MemberRecharges
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})

	memberRecharges.UID = int(user["ID"].(float64))
	memberRecharges.Username = user["Username"].(string)
	memberRecharges.RechargeID = ID
	memberRecharges.ActionIP = ctx.RemoteAddr()
	memberRecharges.Amount = float64(amountInt)
	memberRecharges.RechargeModel = 0 // 人工匯款
	memberRecharges.RechargeComID = bankidInt
	memberRecharges.RechargeComName = paramsBanks.BankName
	memberRecharges.RechargeModelName = lang.Lc.Tr("msg1")
	memberRecharges.Status = 0
	memberRecharges.Memo = memo

	if err := db.DB.Create(&memberRecharges).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err31"))
		return
	}
	comm.Json(ctx, iris.Map{"rechargeId": ID})
	return

}

// transfer_logs
func RechargeThird(ctx iris.Context) {
	var code = ctx.FormValue("code")
	var amount = ctx.FormValue("amount")
	var thirdID = ctx.FormValue("thirdID")
	if code == "" || thirdID == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err28"))
		return
	}
	thirdIDInt, _ := strconv.Atoi(thirdID)
	amountInt, err := strconv.Atoi(amount)
	if err != nil || amountInt <= 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err27"))
		return
	}

	var paramsThirdpays model.ParamsThirdpays
	db.DB.First(&paramsThirdpays, "payin_status = 1 and id = ?", thirdIDInt)
	if paramsThirdpays.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err28"))
		return
	}
	if amountInt < paramsThirdpays.MinAmount {
		comm.JsonErr(ctx, lang.Lc.Tr("err29"))
		return
	}
	if amountInt > paramsThirdpays.MaxAmount {
		comm.JsonErr(ctx, lang.Lc.Tr("err30"))
		return
	}
	var bankLists model.BankLists
	db.DB.First(&bankLists, "for_recharge = 1 and enable = 1 and ename = ?", code)
	if bankLists.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err28"))
		return
	}
	ID := comm.OrderID(db.DB)
	var memberRecharges model.MemberRecharges
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})

	memberRecharges.UID = int(user["ID"].(float64))
	memberRecharges.Username = user["Username"].(string)
	memberRecharges.RechargeID = ID
	memberRecharges.ActionIP = ctx.RemoteAddr()
	memberRecharges.Amount = float64(amountInt)
	memberRecharges.RechargeModel = 1 // 三方支付
	memberRecharges.RechargeComID = thirdIDInt
	memberRecharges.RechargeComName = bankLists.Name
	memberRecharges.RechargeModelName = lang.Lc.Tr("msg2")
	memberRecharges.Status = 0

	url := getThirdUrl(map[string]interface{}{
		"thirdID": thirdID,
		"code":    code,
		"amount":  amountInt,
		"orderID": ID,
	})

	if err := db.DB.Create(&memberRecharges).Error; err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err31"))
		return
	}
	comm.Json(ctx, iris.Map{"rechargeId": ID, "url": url})
	return

}

func getThirdUrl(data map[string]interface{}) string {
	fmt.Println(data)
	return "test_url"
}

// RechargeLog
func RechargeLog(ctx iris.Context) {
	var memberRecharges []model.MemberRecharges
	var page = ctx.FormValue("page")
	//var status = ctx.FormValue("status")
	var fromTime = ctx.FormValue("fromTime")
	var toTime = ctx.FormValue("toTime")

	if page == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	// statusInt, err := strconv.Atoi(status)
	// if err != nil {
	// 	comm.JsonErr(ctx, lang.Lc.Tr("err12"))
	// 	return
	// }
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	table := "member_recharges"
	Db := db.DB
	Db = Db.Table(table).Where("uid = ? and created_at >= ? and created_at <= ?", uid, fromTime+" 00:00:00", toTime+" 23:59:59")
	// if statusInt > 0 {
	// 	Db = Db.Table(table).Where("type = ?", statusInt)
	// }
	Db.Table(table).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Scan(&memberRecharges)
	comm.Json(ctx, memberRecharges)
	return
}

// CashLog
func CashLog(ctx iris.Context) {
	var memberCashs []model.MemberCashs
	var page = ctx.FormValue("page")
	//var status = ctx.FormValue("status")
	var fromTime = ctx.FormValue("fromTime")
	var toTime = ctx.FormValue("toTime")

	if page == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	// statusInt, err := strconv.Atoi(status)
	// if err != nil {
	// 	comm.JsonErr(ctx, lang.Lc.Tr("err12"))
	// 	return
	// }
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	table := "member_cashs"
	Db := db.DB
	Db = Db.Table(table).Where("uid = ? and created_at >= ? and created_at <= ?", uid, fromTime+" 00:00:00", toTime+" 23:59:59")
	// if statusInt > 0 {
	// 	Db = Db.Table(table).Where("type = ?", statusInt)
	// }
	Db.Table(table).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Scan(&memberCashs)
	comm.Json(ctx, memberCashs)
	return
}

// 站內信
func Message(ctx iris.Context) {
	var page = ctx.FormValue("page")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	var messageReceivers []model.MessageReceivers
	db.DB.Preload("MessageSender", func(db *gorm.DB) *gorm.DB {
		return db.Select("id", "title", "content")
	}).Where("uid = ? ", uid).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&messageReceivers)
	var data []map[string]interface{}
	for _, value := range messageReceivers {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"IsReaded":  value.IsReaded,
			"CreatedAt": value.CreatedAt,
			"Title":     value.MessageSender.Title,
			"Content":   value.MessageSender.Content,
		})
	}
	comm.Json(ctx, data)
	return
}

// 站內信改成已讀
func MessageRead(ctx iris.Context) {
	var id = ctx.FormValue("id")
	if id == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))
	var messageReceivers model.MessageReceivers
	db.DB.Model(&messageReceivers).Where("id = ? and uid = ?", id, uid).Update("is_readed", 1)
	comm.Json(ctx, nil)
	return
}

// 站內信
func CheckLetter(ctx iris.Context) {
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	var messageReceivers model.MessageReceivers
	db.DB.Where("is_readed = ? and uid = ?", 0, uid).First(&messageReceivers)
	comm.Json(ctx, messageReceivers.ID)
	return
}

// transfer_logs
func TransLog(ctx iris.Context) {
	var transLog []model.TransferLog
	var page = ctx.FormValue("page")
	var status = ctx.FormValue("status")
	var fromTime = ctx.FormValue("fromTime")
	var toTime = ctx.FormValue("toTime")

	if page == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	statusInt, err := strconv.Atoi(status)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	table := "transfer_logs_tmp" + strconv.Itoa(uid%10)
	Db := db.DB
	Db = Db.Table(table).Where("uid = ? and created_at >= ? and created_at <= ?", uid, fromTime+" 00:00:00", toTime+" 23:59:59")
	if statusInt > 0 {
		Db = Db.Table(table).Where("type = ?", statusInt)
	}
	Db.Table(table).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Scan(&transLog)
	//db.DB.Table(table).Where("uid = ?", int(uid.(float64))).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Scan(&transLog)
	comm.Json(ctx, transLog)

	// ctx.JSON(iris.Map{
	// 	"status": 1,
	// 	"data":   transLog,
	// })
	return
}
func ProdLog(ctx iris.Context) {
	var memberProds []model.MemberProds
	var page = ctx.FormValue("page")
	var status = ctx.FormValue("status")
	var fromTime = ctx.FormValue("fromTime")
	var toTime = ctx.FormValue("toTime")
	if page == "" {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}
	statusInt, err := strconv.Atoi(status)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	//db.DB.Where("uid = ?", int(uid.(float64))).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&bidlog)
	table := "member_prods"
	Db := db.DB
	Db = Db.Table(table).Where("uid = ? and created_at >= ? and created_at <= ?", uid, fromTime+" 00:00:00", toTime+" 23:59:59")
	if statusInt < 4 {
		Db = Db.Table(table).Where("status = ?", statusInt)
	}
	Db.Table(table).Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Scan(&memberProds)
	// for _, v := range bidlog {
	// 	fmt.Println(v)
	// }
	comm.Json(ctx, memberProds)
	return
}
