package agent

import (
	"strconv"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	"gorm.io/gorm"
	"iris.wa/comm"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/lang"
	"iris.wa/model"
)

func AgentRebate(ctx iris.Context) {
	var page = ctx.FormValue("page")
	var fromTime = ctx.FormValue("fromTime")
	var toTime = ctx.FormValue("toTime")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	var memberReports []model.MemberReports

	DbSQL := db.DB.Where("uid = ?", uid)
	if fromTime != "" && toTime != "" {
		DbSQL.Where("actionDate >= ? and actionDate <= ?", fromTime+" 00:00:00", toTime+" 23:59:59")
	}
	DbSQL.Order("actionDate desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&memberReports)
	var data []map[string]interface{}
	for _, value := range memberReports {
		if value.Rebate > 0 {
			data = append(data, map[string]interface{}{
				"Rebate":    value.Rebate,
				"CreatedAt": value.ActionDate,
			})
		}
	}
	comm.Json(ctx, data)
	return
}

func AgentQR(ctx iris.Context) {
	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))

	var inviteLinks model.InviteLinks
	db.DB.First(&inviteLinks, "uid = ? and enable = 1", uid)
	if inviteLinks.UID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	comm.Json(ctx, map[string]interface{}{
		"code": inviteLinks.Code,
		"used": inviteLinks.Used,
	})
	return
}

// 檢查用戶狀態
func Team(ctx iris.Context) {
	var page = ctx.FormValue("page")
	var fromTime = ctx.FormValue("fromTime")
	var toTime = ctx.FormValue("toTime")
	var username = ctx.FormValue("username")

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})

	var members []model.Members
	uid := int(user["ID"].(float64))
	//Db := db.DB
	// db.DB.Preload("InviteLink", func(db *gorm.DB) *gorm.DB {
	// 	return db.Select("id", "uid", "used")
	// }).Where("parent_id = ? and created_at >= ? and created_at <= ?", uid, fromTime+" 00:00:00", toTime+" 23:59:59").Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&members)
	DbSQL := db.DB.Preload("InviteLink", func(db *gorm.DB) *gorm.DB {
		return db.Select("id", "uid", "used")
	}).Where("parent_id = ?", uid)
	if fromTime != "" && toTime != "" {
		DbSQL.Where("created_at >= ? and created_at <= ?", fromTime+" 00:00:00", toTime+" 23:59:59")
	}
	if username != "" {
		DbSQL.Where("username like ?", "%"+username+"%")
	}
	DbSQL.Order("created_at desc").Limit(conf.Conf.PageSize).Offset((pageInt - 1) * conf.Conf.PageSize).Find(&members)
	var data []map[string]interface{}
	for _, value := range members {
		data = append(data, map[string]interface{}{
			"ID":        value.ID,
			"Username":  value.Username,
			"LoginAt":   value.LoginAt,
			"CreatedAt": value.CreatedAt,
			"Used":      value.InviteLink.Used,
		})
	}
	comm.Json(ctx, data)
	return
}

// 檢查獲取用戶近3個月消費額
func MemberDetial(ctx iris.Context) {
	var id = ctx.FormValue("id")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		comm.JsonErr(ctx, lang.Lc.Tr("err12"))
		return
	}

	session := sessions.Get(ctx)
	user := session.Get("user").(map[string]interface{})
	uid := int(user["ID"].(float64))
	var members model.Members
	db.DB.First(&members, "id = ? and parent_id = ?", idInt, uid)
	if members.ID == 0 {
		comm.JsonErr(ctx, lang.Lc.Tr("err7"))
		return
	}
	now := time.Now()
	oldTime := now.AddDate(0, 0, -90) //若要获取3天前的时间，则应将-2改为-3
	t1, _ := time.ParseInLocation("2006-01-02", now.Format("2006-01-02"), time.Local)
	t2, _ := time.ParseInLocation("2006-01-02", oldTime.Format("2006-01-02"), time.Local)
	var bid float64
	db.DB.Raw("SELECT IFNULL(sum(bid),0) FROM member_reports WHERE uid = ? and actionDate >= ? and actionDate <= ?", idInt, t2, t1).Scan(&bid)
	comm.Json(ctx, bid)
	return
}
