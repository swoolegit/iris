module agent

go 1.14

require (
	github.com/kataras/iris/v12 v12.1.8
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	gorm.io/gorm v1.20.0
)
