module iris

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/CloudyKit/jet/v5 v5.1.1 // indirect
	github.com/e421083458/golang_common v1.1.2
	//github.com/go-redis/redis v6.15.8+incompatible // indirect
	github.com/go-redis/redis/v8 v8.5.0
	github.com/gorilla/schema v1.1.0 // indirect
	github.com/iafan/Plurr v0.0.0-20200416004027-ca96e77a8f76
	github.com/iafan/go-l10n v0.0.0-20191031185225-4f8f121cf651
	github.com/iris-contrib/formBinder v5.0.0+incompatible // indirect
	github.com/kataras/iris v0.0.2
	//github.com/kataras/iris v0.0.2 // indirect
	//github.com/jinzhu/gorm v1.9.15
	//github.com/kataras/iris v11.1.1+incompatible
	github.com/kataras/iris/v12 v12.2.0-alpha2.0.20210217145719-f34703e3cffb
	github.com/mitchellh/cli v1.1.2 // indirect
	github.com/robfig/cron v1.2.0 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/vmihailenco/tagparser v0.1.2 // indirect
	gorm.io/driver/sqlite v1.0.9 // indirect
	gorm.io/gorm v1.20.7
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc
	iris.wa/backend/activity v0.0.0
	iris.wa/backend/admin v0.0.0
	iris.wa/backend/business v0.0.0
	iris.wa/backend/report v0.0.0
	iris.wa/backend/system v0.0.0
	iris.wa/backend/task v0.0.0
	iris.wa/backend/user v0.0.0
	iris.wa/comm v0.0.0
	iris.wa/conf v0.0.0
	iris.wa/db v0.0.0
	iris.wa/front/agent v0.0.0
	iris.wa/front/game v0.0.0
	iris.wa/front/index v0.0.0
	iris.wa/front/user v0.0.0
	iris.wa/lang v0.0.0
	iris.wa/model v0.0.0
	iris.wa/rdb v0.0.0

)

replace (
	iris.wa/backend/activity => ./api/backend/activity
	iris.wa/backend/admin => ./api/backend/admin
	iris.wa/backend/business => ./api/backend/business
	iris.wa/backend/report => ./api/backend/report
	iris.wa/backend/system => ./api/backend/system
	iris.wa/backend/task => ./api/backend/task
	iris.wa/backend/user => ./api/backend/user
	iris.wa/comm => ./lib/comm
	iris.wa/conf => ./lib/conf
	iris.wa/db => ./lib/db
	iris.wa/front/agent => ./api/front/agent
	iris.wa/front/game => ./api/front/game
	iris.wa/front/index => ./api/front/index
	iris.wa/front/user => ./api/front/user
	iris.wa/lang => ./lang
	iris.wa/model => ./model
	iris.wa/rdb => ./lib/rdb
)
