package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
	"github.com/kataras/iris/v12/sessions/sessiondb/redis"
	"iris.wa/conf"
	"iris.wa/db"
	"iris.wa/front/agent"
	"iris.wa/front/game"
	"iris.wa/front/index"
	"iris.wa/front/user"
	"iris.wa/lang"
)

func main() {
	f := newLogFile()
	defer f.Close()

	app := iris.Default()
	app.Logger().SetOutput(f)

	// 設定conf
	conf.GetInstance().InitConf()

	// DB初始化
	DbStatus := db.GetInstance().InitDataPool(conf.Conf.DB.Source)
	if !DbStatus {
		fmt.Println("failed to connect database:")
		return
	}
	dbForSess := redis.New(redis.Config{
		Network:   "tcp",
		Addr:      getenv("REDIS_ADDR", conf.Conf.Redis.Source),
		Timeout:   time.Duration(600) * time.Second,
		MaxActive: 300,
		Password:  "",
		Database:  conf.Conf.Redis.Database,
		Prefix:    "",
		Driver:    redis.GoRedis(), // redis.Radix() can be used instead.
	})
	//fmt.Println(dbForSess)
	app.Use(myMiddleware)
	// rdb.Set("test", "abcfwefwfewfwfe")
	// Close connection when control+C/cmd+C
	iris.RegisterOnInterrupt(func() {
		dbForSess.Close()
	})

	defer dbForSess.Close() // close the database connection if application errored.
	sess := sessions.New(sessions.Config{
		Cookie:                      "_session_id",
		Expires:                     360 * time.Minute, // defaults to 0: unlimited life. Another good value is: 45 * time.Minute,
		AllowReclaim:                true,
		CookieSecureTLS:             false,
		DisableSubdomainPersistence: false,
	})
	sess.UseDatabase(dbForSess)
	app.Use(sess.Handler())
	// session := sessions.Get(ctx)
	// session.Set("udata", "123")
	//aa := rdb.Get("1077")
	//fmt.Println(aa)
	// Get Russian localization context
	lang.Lc = lang.Loc.GetContext("zh-TW")
	// get translation by key name, then format it using Plurr:

	// app.Handle("GET", "/ping", func(ctx iris.Context) {
	// 	ctx.JSON(iris.Map{"message": "pong"})
	// })
	//user.Reg()
	app.Post("/index/Reg", index.Reg)
	app.Post("/index/Login", index.Login)
	app.Post("/index/Logout", index.Logout)
	app.Post("/index/Bank", index.Bank)
	app.Post("/index/Promotion", index.Promotion)
	app.Post("/index/News", index.News)
	app.Post("/index/VideoLast", index.VideoLast)
	app.Post("/index/VideoList", index.VideoList)
	app.Post("/index/Bulletin", index.Bulletin)
	// app.Post("/game/Close", game.Close)
	// app.Post("/game/Bonus", game.Bonus)

	users := app.Party("/user", checkSession)
	users.Post("/BidLog", user.BidLog)
	users.Post("/ProdLog", user.ProdLog)
	users.Post("/TransLog", user.TransLog)
	users.Post("/CheckLogin", user.CheckLogin)
	users.Post("/Safe", user.Safe)
	users.Post("/SafeUp", user.SafeUp)
	users.Post("/RechargeInfo", user.RechargeInfo)
	users.Post("/RechargeBank", user.RechargeBank)
	users.Post("/RechargeThird", user.RechargeThird)
	users.Post("/RechargeLog", user.RechargeLog)
	users.Post("/Cash", user.Cash)
	users.Post("/CashLog", user.CashLog)
	users.Post("/Message", user.Message)
	users.Post("/MessageRead", user.MessageRead)
	users.Post("/CheckLetter", user.CheckLetter)

	games := app.Party("/game", checkSession)
	games.Post("/Bid", game.Bid)
	games.Post("/Prod", game.Prod)

	agents := app.Party("/agent", checkSession)
	agents.Post("/Team", agent.Team)
	agents.Post("/MemberDetial", agent.MemberDetial)
	agents.Post("/AgentQR", agent.AgentQR)
	agents.Post("/AgentRebate", agent.AgentRebate)

	//app.DoneGlobal(after)
	// Listens and serves incoming http requests
	// on http://localhost:8080.
	srv := &http.Server{
		Addr:         ":7575",
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 120 * time.Second,
	}
	srv.SetKeepAlivesEnabled(false)
	app.Run(iris.Server(srv))
	//app.Listen(":7171")

}

func checkSession(ctx iris.Context) {
	session := sessions.Get(ctx)
	user := session.Get("user")
	if user == nil {
		ctx.JSON(iris.Map{
			"status": 0,
			"msg":    lang.Lc.Tr("err9"),
			"func":   1,
		})
		return
	}
	ctx.Next()
}

func myMiddleware(ctx iris.Context) {
	ctx.Header("Access-Control-Allow-Origin", "*")
	ctx.Header("Access-Control-Allow-Headers", "*")
	ctx.Header("Access-Control-Allow-Methods", "*")
	ctx.Header("Access-Control-Allow-Credentials", "true")
	ctx.Application().Logger().Infof("Runs before %s", ctx.Path())
	ctx.Next()
}

func todayFilename() string {
	today := time.Now().Format("2006-01-02")
	return "./log/" + today + ".txt"
}

func newLogFile() *os.File {
	filename := todayFilename()
	// Open the file, this will append to the today's file if server restarted.
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return f
}
func getenv(key string, def string) string {
	if v := os.Getenv(strings.ToUpper(key)); v != "" {
		return v
	}

	return def
}
